\babel@toc {english}{}
\contentsline {chapter}{Acknowledgments}{I}{chapter*.3}% 
\contentsline {chapter}{Sommario}{II}{chapter*.4}% 
\contentsline {chapter}{Abstract}{III}{chapter*.5}% 
\contentsline {chapter}{Nomenclature}{X}{chapter*.9}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}State of the art}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Work justification and purposes}{4}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Proposed approach}{4}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Thesis structure}{5}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Machine Learning Theory}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Supervised Machine Learning}{6}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Imitation Learning}{7}{section.2.2}% 
\contentsline {section}{\numberline {2.3}The Markov Property}{8}{section.2.3}% 
\contentsline {section}{\numberline {2.4}DAgger}{8}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Artificial Neural Networks}{10}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Extreme Learning Machine}{14}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Performance Indexes}{16}{section.2.7}% 
\contentsline {chapter}{\numberline {3}Energy Optimal Landing Problem}{20}{chapter.3}% 
\contentsline {section}{\numberline {3.1}ZEM/ZEV}{21}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Choice of the input features}{23}{section.3.2}% 
\contentsline {section}{\numberline {3.3}1D case}{24}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Problem formulation}{24}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Train and test dataset generation}{25}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Dynamics simulator}{26}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}DAgger procedure}{27}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Visualize DAgger benefits}{29}{subsection.3.3.5}% 
\contentsline {subsection}{\numberline {3.3.6}Deep Neural Network}{31}{subsection.3.3.6}% 
\contentsline {subsection}{\numberline {3.3.7}Extreme Learning Machine}{35}{subsection.3.3.7}% 
\contentsline {section}{\numberline {3.4}3D case}{38}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Problem formulation}{38}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}Train and test dataset generation}{39}{subsection.3.4.2}% 
\contentsline {subsection}{\numberline {3.4.3}Dynamics simulator}{40}{subsection.3.4.3}% 
\contentsline {subsection}{\numberline {3.4.4}DAgger procedure}{41}{subsection.3.4.4}% 
\contentsline {subsection}{\numberline {3.4.5}Visualize DAgger benefits}{42}{subsection.3.4.5}% 
\contentsline {subsection}{\numberline {3.4.6}Deep Neural Network}{43}{subsection.3.4.6}% 
\contentsline {subsection}{\numberline {3.4.7}Extreme Learning Machine}{46}{subsection.3.4.7}% 
\contentsline {chapter}{\numberline {4}Fuel Optimal Landing Problem}{49}{chapter.4}% 
\contentsline {section}{\numberline {4.1}GPOPS}{50}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Choice of the input features}{51}{section.4.2}% 
\contentsline {section}{\numberline {4.3}1D case}{52}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Problem formulation}{52}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Train and test dataset generation}{53}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}Dynamics simulator}{56}{subsection.4.3.3}% 
\contentsline {subsection}{\numberline {4.3.4}DAgger procedure}{56}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Visualize DAgger benefits}{58}{subsection.4.3.5}% 
\contentsline {subsection}{\numberline {4.3.6}Deep Neural Network}{59}{subsection.4.3.6}% 
\contentsline {subsection}{\numberline {4.3.7}Extreme Learning Machine}{62}{subsection.4.3.7}% 
\contentsline {section}{\numberline {4.4}3D case}{63}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Problem formulation}{63}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Train and test dataset generation}{64}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}Dynamics simulator}{65}{subsection.4.4.3}% 
\contentsline {subsection}{\numberline {4.4.4}DAgger procedure}{66}{subsection.4.4.4}% 
\contentsline {subsection}{\numberline {4.4.5}Deep Neural Network}{66}{subsection.4.4.5}% 
\contentsline {chapter}{\numberline {5}Conclusions and future work}{70}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Conclusions}{70}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Future work}{71}{section.5.2}% 
\contentsline {chapter}{Bibliography}{72}{chapter*.59}% 
