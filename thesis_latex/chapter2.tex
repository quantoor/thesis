\chapter{Machine Learning Theory}
\label{chapter2}
\thispagestyle{empty}

% \begin{quotation}
% {\footnotesize
% \noindent{\emph{``Bud: Apri!\\
% Cattivo: Perche', altrimenti vi arrabbiate?\\
% Bud e Terence: Siamo gia' arrabbiati!''}}
% \begin{flushright}
% Altrimenti ci arrabbiamo
% \end{flushright}
% }
% \end{quotation}
% \vspace{0.5cm}
In this chapter are presented the theoretical aspects of Machine Learning that are required to better understand what is done in the next chapters.


\vspace{0.5cm}
\section{Supervised Machine Learning}
Machine Learning (ML) is a particular branch of Artificial Intelligence (AI), and it aims at making computer systems able to perform high-level complex tasks effectively and without receiving explicit instructions, relying instead on patterns learned from data. Machine Learning systems can be classified according to the amount and type of supervision they get during training. There are four major categories: \textit{supervised learning}, \textit{unsupervised leaning}, \textit{semisupervised learning} and \textit{reinforcement learning}. In supervised learning, the training data fed to the model includes both the input features $\bm{X}_i$ and the desired solutions $\bm{y}_i$, called \textit{labels}. The train set is thus constituted by the pairs $\{\bm{X}_i, \bm{y}_i\}$, $i = 1,\hdots,N$, where N is the number of samples in the train set. The goal of the training is making the model able to learn the relation between the input features and the target labels, so that it is able to generalize on new, never seen, inputs. A model is said to \textit{overfit} if it does perform well on the train data, but poorly on never seen data \cite{hands_on}, \cite{deep_learning}. Instead, it is said to \textit{underfit} if does not perform well on the train data either. In this work supervised learning algorithms are used, and the pairs state-actions will be provided to the learner during training.

It is possible to make a further distinction of the problem: if the label $\bm{y}$ is categorical, it is a \textit{classification} problem, while if it is numerical, it is a \textit{regression} problem. It will be seen every combination of these problems: only regression (EO problem 1D and 3D), only classification (FO problem 1D) and both classification and regression (FO problem 3D).


\section{Imitation Learning}
\label{sec:imitation_learning}
Imitation learning is a control design paradigm that seeks to learn a control policy reproducing demonstrations from experts. For the landing problem studied in this work, the expert is a software which generates optimal trajectories. The pairs of states and control actions are fed to the ML model during training, so that it learns to map a given state to an optimal control action. This makes the type of Machine Learning supervised.

Consider the following scenario: an agent, which is the ML model, receives in input some \textit{observations} $\bm{o}_t$, and takes an \textit{action} $\bm{u}_t$ basing its decision on a \textit{policy} $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$, which can be deterministic or stochastic (in this work a deterministic policy is trained) and depends on some parameters $\theta$. The actions have an effect on future observation. The goal of imitation learning is to train the best policy possible, in order to perform the optimal actions for each observation, learning from what is called the \textit{expert policy}. The expert can be a human, or in this case a software which solves the optimal landing problem and generates optimal trajectories.


\section{The Markov Property}
\label{sec:markov_property}
A process has the \textit{Markov property} if the conditional probability distribution of future states of the process (conditional on both past and present states) depends only upon the present state, not on the sequence of events that preceded it. This property can be formalized as following:

\begin{equation}
P(s_t \ | \ s_{t-1}, s_{t-2}, \hdots, s_1) = P(s_t \ | \ s_{t-1})
\end{equation}

\noindent where P is the probability of happening the state s at time t, given the states at previous times. Thus, the full knowledge of the dynamics of the system, and so of the state $\bm{x}_t$ other than the equations of motion, is required to assume the Markov property. The observation indicates the quantity of information that is available to the agent, and is in general different from the state. In this work it is assumed complete knowledge of the state, so that there is no distinction between the state $\bm{x}_t$ and the observation $\bm{o}_t$, and in the following sections they will be used interchangeably. The Markov assumption is important because it allows to use models like DNNs and ELMs, which take only one observation at a time, and are not able to reconstruct information from previous observations like recurrent neural networks (RNNs).



\section{DAgger}
\label{sec:dagger}
Dataset Aggregation (DAgger) techniques are already used in Machine Learning, and they usually consist in modifying slightly the train data (e.g. adding noise), and augmenting with that new artificial data the train set. For the sequential prediction problem, instead, DAgger works in a different way, and it aims at solving a very specific issue. As already observed in section \ref{sec:work_justification_and_purposes}, sequential prediction problems violate the i.i.d. assumption because future observations depend on previous predictions. Taking as a reference fig. \ref{fig:trajectory_diverge}, the model is trained on the distribution of data coming from the expert policy $p_{data}(\bm{o}_t)$, but at simulation time it encounters a distribution of data coming from a different policy ${p_{\pi}}_{\theta}(\bm{o}_t)$ of the model. In fact, the distribution of observations induced by ${p_{\pi}}_{\theta} (\bm{o}_t)$ is not the same as the distribution of observations from which the train data came from, because of the prediction errors.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{img/trajectory_diverge.png}
    \caption{The issue of sequential prediction problems in imitation learning. Image taken from \cite{levine_dagger}.}
    \label{fig:trajectory_diverge}
\end{figure}

\noindent To this problem, there are in particular two possible solution: one is training a very good policy, which is the same of the expert policy, but this is very difficult to achieve; a simpler solution is to use DAgger, whose purpose is to make the distribution of data encountered at prediction time, the same on which the model has been trained ($p_{data}(\bm{o}_t) = {p_{\pi}}_{\theta} (\bm{o}_t)$). This is done just running the policy $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$ and augmenting the train data with the observations encountered during the simulation. The DAgger algorithm can be summarized as follow:

\begin{enumerate}
	\item Train $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$ from human data $\mathbb{D} = \{ \bm{o}_1, \bm{u}_1, \hdots, \bm{o}_N, \bm{u}_N \}$
	\item Run $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$ to get dataset $\mathbb{D}_{\pi} = \{ \bm{o}_1, \hdots, \bm{o}_N \} $
	\item Ask human to label $\mathbb{D}_{\pi} $ with actions $\bm{u}_t$
	\item Aggregate: $\mathbb{D} \leftarrow \mathbb{D} \cup \mathbb{D}_{\pi}$ and return to point 1.
\end{enumerate}

\noindent The DAgger algorithm has already been applied to situations where the model is supposed to learn from human behavior, for example in problem such as driving car \cite{self_driving_car} or video games \cite{super_tux_kart}, \cite{mario_bros}. In the landing problem, however, a human cannot label the observations with optimal actions; instead, a software is used to generate optimal trajectories: ZEM/ZEV for the EO problem, and GPOPS for the FO problem. The following algorithm is then applied:

\begin{enumerate}
	\item Train $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$ from software data $\mathbb{D} = \{ \bm{o}_1, \bm{u}_1, \hdots, \bm{o}_N, \bm{u}_N \}$
	\item Run $\pi_{\theta} (\bm{u}_t|\bm{o}_t)$ to get dataset $\mathbb{D}_{\pi} = \{ \bm{o}_1, \hdots, \bm{o}_N \} $
	\item Use the software to label $\mathbb{D}_{\pi} $ with actions $\bm{u}_t$
	\item Aggregate: $\mathbb{D} \leftarrow \mathbb{D} \cup \mathbb{D}_{\pi}$ and return to point 1.
\end{enumerate}


\section{Artificial Neural Networks}
Artificial Neural Networks (ANNs) are a Machine Learning model that take inspiration from the biological brain's architecture. They are versatile, powerful and scalable, characteristics that make them ideal for highly complex AI tasks.
% To better understand the ANNs, it is useful to look at their analogy with the biological neuron, which is represented in fig. \ref{fig:biological_neuron}:

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.8\linewidth]{img/biological_neuron.png}
%     \caption{Biological neuron}
%     \label{fig:biological_neuron}
% \end{figure}

% \noindent The biological neuron is composed of a \textit{cell body}, containing the \textit{nucleus} and other components, many branching extensions called \textit{dendrites}, and one long extension called the \textit{axon} Near its extremity the axon splits off into many branches, and at the tip of these branches are minuscule structures called \textit{synapses}, which are connected to the dendrited (or directly to the cell body) of other neurons. Biological neurons receive short electrical impulses called \textit{signals} from other neurons through the synapses. \textit{When a neuron receives a sufficient number of signals from other neurons within a few milliseconds, it fires its own signal}.

Artificial neural networks can be grouped in \textit{feedforward neural networks} and \textit{recurrent neural networks}, depending on the structure of the connections between the neurons. Recurrent neural networks are structured in a way that some neurons receive in input their own output, plus the input of other neurons, and this makes these kind of networks ideal to process sequences of input and extract information about their history. If there are no such feedback connections, the network is classified as a feedforward neural network. The simplest feedforward architecture is the \textit{Single Layer Feedorward Network} (SLFN), which is composed by an \textit{input layer}, a \textit{hidden layer} and an \textit{output layer}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{img/feedforward_recurrent.png}
    \caption{Scheme of recurrent and feedforward neural networks. Image taken from \cite{feedforward_recurrent}.}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{img/SLFN.png}
    \caption{Scheme of a SLFN. Image taken from \cite{SLFN}.}
    \label{fig:SLFN}
\end{figure}

\noindent In general, the output of function of a SLFN can be written as:
\begin{equation}
\bm{f}_L (\bm{x}) = \sum_{i=1}^{L} \bm{\beta}_i h_i (\bm{x}, \bm{w}_i, b_i)
\label{eq:sfln_output}
\end{equation}

\noindent where L is the number of neurons in the hidden layer, $\bm{\beta}_i$ with $i = 1, \hdots, L$ is the output weights vector of the i-th node, $\bm{w}_i$ and $b_i$ are respectively the input weights vector and the bias, $\bm{x}$ is the input vector. Functions $h_i$ are the activation functions of the neurons and are in general non linear piece-wise continuous. Their purpose is to introduce in the network a non linearity, which is necessary for the model to represent complex non linear dynamics. Different activation functions have different behavior: for example, they can be discontinuous in the zero (step), continuous but not differentiable (ReLU) or continuous and differentiable (sigmoid and tanh); furthermore, they can be saturating (step, sigmoid, tanh, ReLU for negative inputs) if the output is bounded, or non saturating (ReLU for positive inputs) if the output is not bounded. These properties ultimately affects the model behavior and the activation functions have to be chosen for each problem. A list of activation functions commonly used in ML is reported in tab. \ref{tbl:activations}.

\vspace{0.5cm}

\begin{table}[H]
    \begin{center}
        \begin{tabular}{ | p{2.5cm} | p{3cm} | p{4.5cm} | p{2.5cm} | }
        \hline
        Activation & Formula & Plot & Output range\\ 
        \hline

        Step
        &
        $$
        h(x) = 
        \begin{cases}
            0 \ x < 0\\
            1 \ x \geq 1\\
        \end{cases}
        $$
        &
        \raisebox{-\totalheight}{\includegraphics[width=0.3\textwidth, height=20mm]{img/activations_step.png}} 
        & $h = 0 \lor h = 1$\\

        \hline

        ReLU (rectified linear unit)
        &
        $$ h(x) = \text{max} (0, x) $$
        &
        \raisebox{-\totalheight}{\includegraphics[width=0.3\textwidth, height=20mm]{img/activations_relu.png}}
        & $h \in [0, \infty)$\\

        \hline

        Sigmoid
        &
        $$h(x) = \frac{1}{1+e^{-x}}$$
        &
        \raisebox{-\totalheight}{\includegraphics[width=0.3\textwidth, height=20mm]{img/activations_sigmoid.png}}
        & $h \in (0,1)$\\


        \hline
        Tanh (hyperbolic tangent)
        &
        $$h(x) = \frac{e^x-e^{-x}}{e^x+e^{-x}}$$
        &
        \raisebox{-\totalheight}{\includegraphics[width=0.3\textwidth, height=20mm]{img/activations_tanh.png}}
        & $h \in (-1,1)$\\

        \hline
        \end{tabular}
    \caption{Common activation functions}
    \label{tbl:activations}
    \end{center}
\end{table}

\noindent \textit{Deep neural networks} are feedforward neural networks with two ore more hidden layers, and every layer except the output layer is fully connected to the next layer. Training a neural networks it means finding the values of the weights and biases such that, given an input, the desired output is obtained. The training is done with an algorithm called \textit{backpropagation}, which can be described as a gradient descent algorithm with reverse-mode autodiff. It works as follows: for each training instance, the backpropagation algorithm first makes a prediction computing the output of every neuron in each consecutive layer (forward pass), than measures the error (it knows the true label, since it is supervised learning), and then goes back through each layer to measure the error contribution from each connection (reverse pass), and finally tweaks the connection weights to reduce the error (gradient descent step). 

There are some parameters, called \textit{hyperparameters}, which can be chosen and are fundamental in determining the quality and success of the training. The most common and important hyperparameters are:

\begin{itemize}
    \item Number of hidden layers: having more hidden layers allows the model to represent more complex structures. Too many hidden layers may lead to overfitting and slow all the computations (training and prediction), but not enough layers may not be adequate to represent the complexity of the problem (underfit).

    \item Number of neurons per layer: the number of neurons in the input and output layer are determined by the input and output size. The number of neurons in the hidden layers are hyperparameters, and like the number of layers, too many of them cause overfitting, but not enough underfitting.

    \item Activations: depending on the problem, it may be desirable to choose a saturating activation (like step, sigmoid or tanh) or a non saturating one (like ReLU, for positive inputs).

    \item Learning rate: influences the speed of the weight update. If it is too high, the training is very fast but may not reach convergence. If it is too low, the training is too slow and may be stuck in a local minimum.

    \item Mini-batch size: indicates the number of samples fed to the network at each iteration to update the weights.

    \item Normalization: all the inputs are rescaled within a specified range. In this way, it is avoided having input features with different orders of magnitude, which in general helps the training.
\end{itemize}

\noindent Unfortunately, there are only a few rule of thumbs, but other than, that there is no strict rule to choose the hyperparameters. They absolutely depend on the nature and kind of problem that has to be solved, so a trial and error procedure is required.

% The \textit{Perceptron} is one of the simplest form of ANN. It is based on an artificial neuron called \textit{linear threshold unit} (LTU). The LTU has a certain number of inputs $x_i$, each of one is associated to a weight $w_i$, and an output $h_w$. What the LTU does is to apply a step function to the weighted sum of its inputs, so that the output is:

% \begin{equation}
% 	h_w(\bm{x}) = \text{step} \left( \sum_i w_i \cdot x_i \right) = \text{step} ( \bm{w}^T \bm{x})
% \end{equation}

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.6\linewidth]{img/LTU.png}
%     \caption{Linear threshold unit}
%     \label{fig:LTU}
% \end{figure}

% \noindent It is now easier to understand the analogy between biological and artifical networks: if the LTU is connected to other LTUs, it can be considered as a neuron connected to other neurons, which may send or not a signal depending on the quantity of signals they receive in input. From now on, it will be referred to the artificial neurons as neurons.

% The Perceptron of a single layer of LTUs, with each neuron connected to all the inputs.

% \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.7\linewidth]{img/perceptron.png}
%     \caption{Perceptron}
%     \label{fig:perceptron}
% \end{figure}

% \begin{equation}
% w_{i,j} = w_{i,j} + \eta ( y_j - \hat{y}_j ) x_i
% \end{equation}



\section{Extreme Learning Machine}
Extreme learning machines are a type of single layer feedforward networks, but their training process is completely different from classical ANNs. In fact, it has been proved by Huang et al. \cite{elm_paper} that if a SLFN with tunable hidden nodes parameters can learn a regression of a target function $\bm{f}(\bm{x})$, then, if the hidden nodes activation functions $h_i(\bm{x}, \bm{w}_i, b_i)$, $i = 1 \hdots L$ are non-linear piecewise continuous, training of the network does not require tuning of those parameters. This means that input weights $\bm{w}_i$ and biases $b_i$ of hidden nodes can be assigned randomly, and a SLFN will still maintain the property of universal approximator, as long as the output weights $\bm{\beta}_i$ are computed properly. Referring to eq. \ref{eq:sfln_output} and generalizing to N samples, the output of a SLFN can be expressed as:
\begin{equation}
\bm{y}_i = \sum_{i=1}^{L} \bm{\beta}_i h_i (\bm{x}_j, \bm{w}_i, b_i) \hspace{1cm} j = 1 \hdots N
\label{eq:sfln_output_elm}
\end{equation}

\noindent with $\bm{\beta}_i \in \mathbb{R}^{m_{out} \times 1}$ and $\bm{w}_i \in \mathbb{R}^{m_{in} \times 1}$, being $m_{in}$ the input size and $m_{out}$ the output size. Fig. \ref{fig:sfln_elm} represents a SLFN with the connections between input, hidden and output layer. The network is fed a batch of  $N$ samples $\bm{x}_i$ and gives in output $N$ predictions $\bm{y}_i$.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{img/sfln_elm.png}
    \caption{Scheme of a SLFN with connections. Image taken from \cite{barocco}.}
    \label{fig:sfln_elm}
\end{figure}

\noindent The hidden layer matrix of the network is called $\bm{H}$, whose i-th column is the output of the i-th hidden note with respect to the set of inputs $\bm{X} = [\bm{x}_1, \hdots, \bm{x}_N] \in \mathbb{R}^{m_{in} \times N}$:

\begin{equation}
    \bm{H} = 
        \begin{bmatrix}
            h (\bm{x}_1, \bm{w}_1, b_1) & \hdots & h (\bm{x}_1, \bm{w}_L, b_L) \\
            \vdots & \ddots & \vdots \\
            h (\bm{x}_N, \bm{w}_1, b_1) & \hdots & h (\bm{x}_N, \bm{w}_L, b_L) \\ 
        \end{bmatrix}
    , \hspace{1cm} \bm{H} \in \mathbb{R}^{N \times L}
\end{equation}

\noindent Now it is possible to write in matrix formulation eq. \ref{eq:sfln_output_elm}:
\begin{equation}
    \bm{f}_L(\bm{X}) = \bm{Y} = \bm{H} \bm{\beta} \hspace{1cm}
    \text{with} \hspace{5mm} \bm{\beta} = \begin{bmatrix} \bm{\beta}_1^T\\ \vdots \\ \bm{\beta}_L^T \end{bmatrix} \hspace{5mm}
    \bm{\beta} \in \mathbb{R}^{L \times m_{out}}
\end{equation}

\noindent The training algorithm of ELM is aimed to minimize the cost functional $E$ which represents the training error of the SLFN:

\begin{equation}
    E = ||\bm{H} \bm{\beta} - \bm{T}||^2, \hspace{5mm}
    \text{with} \hspace{5mm} \bm{T} = \begin{bmatrix} \bm{t}_1^T\\ \vdots \\ \bm{t}_L^T \end{bmatrix}, \hspace{5mm}
    \bm{T} \in \mathbb{R}^{N \times m_{out}}
\end{equation}

\noindent Where $\bm{T}$ is the matrix collecting the true labels. The trained network is a universal approximator if $\bm{\beta}$ are assigned according to the least square error of the ovedetermined system:
\begin{equation} \bm{H} \bm{\beta} = \bm{T} \end{equation}

\noindent In order to have the solution $\overline{\bm{\beta}}$ with minimum $L_2$ norm among all least squares solutions, it is necessary and sufficient condition to evaluate $\bm{\beta}$ using the Moore-Penrose generalized inverse of the hidden layer matrix $\bm{H}$. Thus, the training algorithm of ELM can be written as:

\begin{equation}
\overline{\bm{\beta}} =\bm{H}^{\dagger} \bm{T} , \hspace{1cm} \bm{H}^{\dagger} = (\bm{H}^T \bm{H})^{-1} \bm{H}^T
\end{equation}

\noindent Note that the training algorithm of ELM does not require iterative tuning as backpropagation does, but it just consists of the evaluation of the pseudo-inverse of $\bm{H}$, usually obtained via single value decomposition with a computational complexity $\mathcal{O}(NL^2)$. For this reason, training an ELM requires in general much less time and computational resources than training a deep network.


\section{Performance Indexes}
% RMSE, accuracy, binary cross-entropy, regression curves, confusion matrix
% \begin{equation} MSE = \frac{1}{N} \sum_{i=1}^{N} (y_i - \hat{y}_i)^2 \end{equation}
Performance indexes are used to verify the quality of the training. The choice of the indexes depends on the problem and on the information that is needed. In this work for the regression problem, it is used the root mean square error:

\begin{equation}
RMSE = \sqrt{\frac{1}{N} \sum_{i=1}^{N} (y_i - \hat{y}_i)^2}
\end{equation}

\noindent The RMSE represents the average error made for every prediction, compared to the true label, and it is used also as loss function to train the DNN. A visual tool used to verify the quality of a regressor is the \textit{regression curve}. An example of regression curve is represented in fig. \ref{fig:regression_curve}: on the x-axis are the target labels, on the y-axis the predictions. The more the predictions are similar to the targets, the more the blue points lie on the orange 45 degrees line.

\vspace{0.5cm}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/regression_curve.png}
    \caption{Example of regression curve}
    \label{fig:regression_curve}
\end{figure}

\noindent For the classification problem instead, as loss function for the training of the DNN is used the \textit{binary cross-entropy}, or log loss:

\begin{equation}
H_p(q) = - \frac{1}{N} \sum_{i=1}^N y_i \cdot log(p(y_i)) + (1-y_i) \cdot log(1 - p(y_i))
\end{equation}

\noindent It is a particular case of the more general cross-entropy loss, but for binary classification. The idea if the cross-entropy is to compute the confidence of the model in predicting a certain class, and penalize the weights associated if it is wrong, or reinforce the weights if it is right, in a way proportional to the confidence of the prediction. As a performance index, it is used the accuracy, which is the ratio between the number of correct predictions over the total number of predictions:

\begin{equation}
\text{Accuracy} = \frac{\text{Number of correct predictions}}{\text{Number of predictions}} = \frac{1}{N} \sum_{i=1}^{N} (1 - ||y_i - \hat{y}_i||)
\end{equation}

\noindent Another tool to evaluate the performance of a classifier is the \textit{confusion matrix}. The idea is to count the number of times that instances of class A are classified as class B.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{img/confusion_matrix.png}
    \caption{Confusion matrix. Image taken from \cite{confusion_matrix}.}
    \label{fig:confusion_matrix}
\end{figure}

\noindent Taking as a reference the confusion matrix for binary classification  reported in fig. \ref{fig:confusion_matrix}: there are two classes, Positive (1) and Negative (0). The confusion matrix counts:
\begin{itemize}
    \item How many Positive values are predicted as Positive (true positive, TP)
    \item How many Negative values are predicted as Negative (true negative, TN)
    \item How many Positive values are predicted as Negative (false negative, FN)
    \item How many Negative values are predicted as Positive (false positive, FP)
\end{itemize}

\noindent With the confusion matrix, it is possible to have a much deeper insight of the classifier's behavior than just looking at the accuracy.