import numpy as np
import pandas as pd
import keras
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dense
from scipy.integrate import ode
import pickle
import datetime
from sklearn.preprocessing import StandardScaler, MinMaxScaler

import sys
sys.path.insert(0, '../../common1D')
import Eom, util, util_plot


def TrainNetwork(TRAIN_DS, MODEL, SCALER, TRAIN_LOG, TRAIN_HISTORY):
    try:
        load_model(MODEL)
        print('Model already exists. Exit training to avoid to overwrite it.\n')
        return
    except:
        pass
    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(TRAIN_DS, header=None)
    X_train = train_ds.iloc[:, :2].values    # state
    y_train = train_ds.iloc[:, 3].values     # thrust min/max

    scaler = MinMaxScaler(feature_range=(0,100))
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)

    pickle.dump(scaler, open(SCALER, "wb"))

    # =============================================================================
    # Parameters
    # =============================================================================
    hidden_neurons = 32
    n_features = 2
    n_classes = 2

    # make label categorical
    y_train = keras.utils.to_categorical(y_train, n_classes) # label for classification

    # =============================================================================
    # DNN for classification and regression
    # =============================================================================
    initializer = keras.initializers.uniform()
    regularizer = keras.regularizers.l2(0.0001)

    input_layer = Input(shape=(n_features,), name='main_input')
    hidden_1 = Dense(hidden_neurons, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(input_layer)
    hidden_2 = Dense(hidden_neurons, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
    output_layer = Dense(n_classes, activation='sigmoid', name='clas_output')(hidden_2)

    model = Model(inputs=[input_layer], outputs=[output_layer])
    # model.summary()

    # =============================================================================
    # Train model
    # =============================================================================
    opt = keras.optimizers.Adam(lr=0.001)

    model.compile(loss={'clas_output': 'binary_crossentropy'},
        metrics={'clas_output':'accuracy'},
        optimizer=opt)

    call_backs_list = [
        keras.callbacks.EarlyStopping(monitor='val_loss', patience=10),
        keras.callbacks.ModelCheckpoint(monitor='val_loss', filepath=MODEL, save_best_only=True),
        keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=3),
        keras.callbacks.CSVLogger(TRAIN_LOG, separator=',', append=False)
        ]

    history = model.fit({'main_input': X_train}, {'clas_output': y_train},
        validation_split=0.15,
        epochs=500, batch_size=8,
        callbacks=call_backs_list)

    pickle.dump(history, open(TRAIN_HISTORY, 'wb'))
    return


def MonteCarloSimulation(MODEL, TEST_DS, PRED_TEST, SCALER, n_steps, dt):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    model = load_model(MODEL)
    scaler = pickle.load(open(SCALER, "rb"))

    opt_ds = pd.read_csv(TEST_DS, header=None)
    opt_ds.columns = ['Position','Velocity','Mass','Thrust','Time']
    X_opt = opt_ds.iloc[:, :3].values.astype('float32')      # state: pos, vel and mass
    time_opt = opt_ds.iloc[:, 4].values.astype('float32')    # time

    n_trajectories = int(X_opt.shape[0]/n_steps);
    predictedTrajectories = []
    finalState_vec = []

    for i in range(n_trajectories):
        j = i*n_steps
        BC = X_opt[j, :]
        time = time_opt[j:j+n_steps]

        predictedTrajectory = PredictTrajectory(model, BC, time[-1], scaler, dt)
        predictedTrajectories.append(predictedTrajectory)
        finalState_vec.append(predictedTrajectory[-1,:2])
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_trajectories))

    predictedTrajectories = np.array(predictedTrajectories)
    np.save(PRED_TEST, predictedTrajectories)

    # save to csv
    predictedTrajectories = util.Convert_npy2csv(PRED_TEST)
    predictedTrajectories_df = pd.DataFrame(predictedTrajectories)
    predictedTrajectories_df.to_csv(PRED_TEST[:-4]+'.csv', header=None, index=None)

    finalState_vec = np.array(finalState_vec)
    finalState_ave = np.sum(abs(finalState_vec), axis=0) / len(finalState_vec)
    print("\nAverage distance of final state from target:")
    print(finalState_ave)
    return None


def PredictTrajectory(model, BC, t_max, scaler, dt):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    n_features = 2
    landed = False

    # First input
    X_i = BC[:n_features].reshape(1, n_features)

    # First prediction
    prediction = model.predict(scaler.transform(X_i))
    y_pred = prediction.argmax(axis=-1) # predicted thrust min/max

    # =============================================================================
    # Initial conditons
    # =============================================================================
    state0 = np.array(BC) # vector of initial conditions

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_state = state0.reshape(1, -1)        # predicted state
    pred_t = y_pred.reshape(-1, 1)            # predicted thrust

    time_int = np.arange(0, t_max+10, dt)
    time_int = time_int.reshape(-1, 1)

    for i in range(len(time_int)-1):
        t0 = time_int[i, 0]
        tf = time_int[i+1, 0]

        x_int = ode(Eom.Eom_GPOPS)
        x_int.set_integrator('dopri5', rtol=1e-10) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(state0, t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(y_pred)
            x_int.integrate(tf, step=True)

        x_new = x_int.y.reshape(1, -1)

        # Make new prediction
        prediction = model.predict(scaler.transform(x_new[:,:-1]))
        y_pred = prediction.argmax(axis=-1)

        pred_state = np.r_[pred_state, x_int.y.reshape(1,-1)]
        pred_t = np.r_[pred_t, y_pred.reshape(1,1)]

        i = i+1
        if x_int.y[0] < 0:
            landed = True
            break

        if x_int.y[1] > 0:
            break

        state0 = x_int.y # define new initial conditions


    # put the vectors in the right form
    pred_state = np.array(pred_state)
    pred_t = np.array(pred_t)
    pred_t = np.reshape(pred_t, [pred_t.size, 1])

    if not landed:
        finalPos = min(pred_state[:, 0])
        print("Not landed, lowest altitude reached: " + str(finalPos))

    print(pred_state[-1,:2])
    return np.c_[pred_state, pred_t, time_int[:i+1]]


def Dagger(plot=0):
    acc = util.GPOPS_CollectBCForDAgger(PRED_TRAJ, TRAIN_DS, CORRECTIONS, SAVE_BC, d_bc, plot=plot)
    results_numpy1 = np.insert(results_numpy, 1, acc)
    util.GPOPS_SaveResults(TEXT_FILE, iter, TRAIN_DS, results_numpy1)
    return None


if __name__ == "__main__":
    iter = '0'

    TRAIN_DS =  'dagger/train_ds/ds_train_' + iter + '.csv'
    SAVE_BC =   'dagger/train_ds/BC_for_dagger_' + iter + '.csv'

    TEST_DS =   '../test_ds/ds_test_' + iter + '.csv'

    PRED_TRAJ = 'dagger/predictions/predictedTrajectories_' + iter + '.npy'
    BC2CORRECT = 'dagger/predictions/bc2correct_' + iter + '.csv'
    CORRECTIONS = 'dagger/predictions/corrections_' + iter + '.csv'

    MODEL =     'dagger/model/DNN_' + iter + '.h5'
    SCALER =   'dagger/model/scaler_DNN_' + iter + '.p'
    TRAIN_LOG = 'dagger/model/training_DNN_' + iter + '.log'
    TRAIN_HISTORY = 'dagger/model/train_history_' + iter + '.p'

    TEXT_FILE =  'dagger/results_' + iter + '.txt'
    FIG_ROOT =   'dagger/img/fig_' + iter + '_'

    n_steps = 100
    dt  = 0.1
    SR_criteria = [7.5, 0.5]
    d_bc = 5


    TrainNetwork(TRAIN_DS, MODEL, SCALER, TRAIN_LOG, TRAIN_HISTORY)
    # util_plot.GPOPS_PlotDecisionBoundary(MODEL, SCALER, FIG_ROOT)
    # util.GPOPS_EvaluateNetwork(TEST_DS, MODEL, SCALERS, TRAIN_HISTORY, FIG_ROOT)
    MonteCarloSimulation(MODEL, TEST_DS, PRED_TRAJ, SCALER, n_steps, dt)
    # util_plot.GPOPS_PartialDependecePlot(MODEL, SCALER)
    # results_numpy = util.GetResults(PRED_TRAJ, TEST_DS, FIG_ROOT, TEXT_FILE, TRAIN_HISTORY, SR_criteria)
