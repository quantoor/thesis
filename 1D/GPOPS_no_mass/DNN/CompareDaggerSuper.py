import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../../common')
import Eom, util, util_plot

def Compare(n_iter):
    dagger_results = np.zeros((n_iter+1, 4))
    super_results = np.zeros((n_iter+1, 4))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_dagger_' + str(i) + '.npy')[:4]
        super_results[i, :] = np.load('super/results_super_' + str(i) + '.npy')[:4]

    x = np.linspace(0, n_iter, n_iter+1)

    plt.figure(num=None, figsize=(10, 7))
    plt.subplot(221)
    plt.grid()
    plt.plot(x, dagger_results[:,0], '-o')
    plt.plot(x, super_results[:,0], '-o')
    plt.legend(['DAgger', 'Supervised'])
    plt.xlabel('Train data')
    plt.ylabel('Average L2')

    plt.subplot(222)
    plt.grid()
    plt.plot(x, dagger_results[:,1], '-o')
    plt.plot(x, super_results[:,1], '-o')
    plt.legend(['DAgger', 'Supervised'])
    plt.xlabel('Train data')
    plt.ylabel('Average RMSE')

    plt.subplot(223)
    plt.grid()
    plt.plot(x, dagger_results[:,2], '-o')
    plt.plot(x, super_results[:,2], '-o')
    plt.legend(['DAgger', 'Supervised'])
    plt.xlabel('Train data')
    plt.ylabel('Average final position')

    plt.subplot(224)
    plt.grid()
    plt.plot(x, dagger_results[:,3], '-o')
    plt.plot(x, super_results[:,3], '-o')
    plt.legend(['DAgger', 'Supervised'])
    plt.xlabel('Train data')
    plt.ylabel('Average final velocity')
    plt.show()


def OnlyDagger(n_iter):
    dagger_results = np.zeros((n_iter+1, 7))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_dagger_' + str(i) + '.npy')[:7]

    x = np.linspace(0, n_iter, n_iter+1)

    fig = plt.figure(num=None, figsize=(10, 9))
    plt.subplot(511)
    plt.grid()
    plt.plot(x, dagger_results[:,0], '-o')
    plt.ylabel('L2')

    plt.subplot(512)
    plt.grid()
    plt.plot(x, dagger_results[:,2], '-o')
    plt.ylabel('Final position')

    plt.subplot(513)
    plt.grid()
    plt.plot(x, dagger_results[:,3], '-o')
    plt.ylabel('Final velocity')

    plt.subplot(514)
    plt.grid()
    plt.plot(x, dagger_results[:,5], '-o')
    plt.ylabel('D_CM_pos')

    plt.subplot(515)
    plt.grid()
    plt.plot(x, dagger_results[:,6], '-o')
    plt.ylabel('D_CM_vel')
    plt.xlabel('Dagger iteration')

    fig.savefig('dagger/img/dagger_results.png')
    plt.show()


def CreateTable(n_iter, train_data_vec):
    dagger_results = np.zeros((n_iter+1, 7))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_dagger_' + str(i) + '.npy')[:7]
    dagger_results = np.c_[np.arange(0,n_iter+1,1), dagger_results, train_data_vec]

    df = pd.DataFrame(dagger_results)
    df.columns = ['Dagger iter','L2','RMSE','Final Pos [m]','Final Vel [m/s]','Good traj','D_CM_pos','D_DM_vel','N train data']
    df.drop(['RMSE','Good traj'],axis=1,inplace=True)
    # pd.set_option('display.float_format', lambda x: '%.1f' % x)
    # pd.options.display.float_format = '{:.2E}'.format
    df['Dagger iter'] = df['Dagger iter'].round(0)
    df['L2'] = df['L2'].round(1)
    df['Final Pos [m]'] = df['Final Pos [m]'].round(1)
    df['Final Vel [m/s]'] = df['Final Vel [m/s]'].round(1)

    fig, ax = util_plot.render_mpl_table(df, header_columns=0, col_width=4.0)
    fig.savefig('dagger/img/dagger_results_tab.png')
    print(df)
    return


if __name__ == "__main__":
    it = 5

    train_data_vec = []
    for i in range(it+1):
        TRAIN_PATH = 'dagger/train_ds/ds_train_dagger_' + str(i) + '.csv'
        temp = pd.read_csv(TRAIN_PATH, header=None)
        train_data_vec.append(temp.shape[0])

    OnlyDagger(it)
    CreateTable(it, train_data_vec)
