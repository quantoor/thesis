import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../common')
import util, util_plot


def PlotTrainingSetDistribution(TRAIN_DS_PATH):
    print('Trajectories in the training set: ' + str(util.GetDatasetDimension(TRAIN_DS_PATH)))

    df = pd.read_csv(TRAIN_DS_PATH, header=None)
    down = df.iloc[:, 0]
    alt = df.iloc[:, 1]

    plt.figure(num=None, figsize=(12, 8))
    n_traj = int(len(down)/150)
    plt.grid()
    plt.title('Distribution of training trajectories')
    for i in range(n_traj):
        plt.plot(down[i*150:i*150+150], alt[i*150:i*150+150], '.', alpha=0.1)
        plt.plot(down[i*150], alt[i*150], 'k.')
    plt.show()



def PlotBCDistribution(TRAIN_DS_PATH):
    df = pd.read_csv(TRAIN_DS_PATH, header=None)
    down = df.iloc[:, 0]
    alt = df.iloc[:, 1]

    plt.figure(num=None, figsize=(12, 8))
    n_traj = int(len(down)/150)
    plt.grid()
    plt.title('Distribution of training trajectories')
    for i in range(n_traj):
        plt.plot(down[i*150], alt[i*150], 'k.')
    plt.show()



if __name__ == "__main__":
    TRAIN_DS_PATH = '../dataset/train_ds/ds_training.csv'
    TRAIN_DAGGER_DS_PATH = '../dataset/train_ds/ds_training_LSTM_dagger_1.csv'

    # PlotTrainingSetDistribution(TRAIN_DS_PATH)
    # PlotTrainingSetDistribution(TRAIN_DAGGER_DS_PATH)
    PlotBCDistribution(TRAIN_DS_PATH)
    PlotBCDistribution(TRAIN_DAGGER_DS_PATH)
