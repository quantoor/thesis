import numpy as np
import pandas as pd
from scipy.integrate import ode
from keras.models import load_model
import pickle

import sys
sys.path.insert(0, '../common')
import util
from Eom import Eom_fun


def PredictTestTrajectories(MODEL_PATH, TEST_DS_PATH, PRED_TRAJ_PATH, n_steps):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    model = load_model(MODEL_PATH)
    scaler = pickle.load(open("scaler_LSTM.p", "rb"))

    Mat_ds = pd.read_csv(TEST_DS_PATH, header=None)
    Mat_ds.columns = ['Altitude','Vertical vel','Mass','Thrust','Time']
    X_ds = Mat_ds.iloc[:, :3].values.astype('float32')      # state
    y_t_ds = Mat_ds.iloc[:, 3].values.astype('float32')
    time_ds = Mat_ds.iloc[:, 4].values.astype('float32')    # time

    n_trajectories = int(X_ds.shape[0]/n_steps);
    predictedTrajectories = []

    for i in range(n_trajectories):
        j = i*n_steps
        X = X_ds[j:j+n_steps, :]
        y_t = y_t_ds[j:j+n_steps]
        time = time_ds[j:j+n_steps]

        predictedTrajectory = PredictTrajectory(model, X, y_t, time, scaler, n_steps-2)

        # util.PlotTrajectory(predictedTrajectory150, X)

        predictedTrajectories.append(predictedTrajectory)
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_trajectories))

    np.save(PRED_TRAJ_PATH, predictedTrajectories)


def PredictTrajectory(model, X_opt, y_t_opt, time_opt, scaler, n_steps):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    mini_batch_size = 3
    n_features = 2

    # first mini batch
    X_i = scaler.transform(X_opt[:mini_batch_size, :n_features])
    X_i = X_i.reshape([1, mini_batch_size, n_features])

    # first prediction
    prediction = model.predict(X_i)
    y_t_pred = prediction[0].argmax(axis=-1) # predicted thrust on/off

    # =============================================================================
    # Initial conditons
    # =============================================================================
    h0 = X_opt[2, 0]  # initial altitude
    vh0 = X_opt[2, 1] # initial vertical velocity
    m0 = X_opt[2, 2]  # initial mass

    in_cond = np.array([h0, vh0, m0]) # vector of initial conditions

    # =============================================================================
    #  Parameters
    # =============================================================================
    g0  = 1.622
    # alpha = 5*10**-4 <== fuuuck!! super hidden bug
    alpha = 1/200/9.81 # 1 / Isp / g0 earth
    T_max  = 3400
    T_min =  1000

    param = [g0, alpha, T_max, T_min, y_t_pred]

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_X = X_opt[:mini_batch_size, :]
    pred_t = np.r_[y_t_opt[:2].reshape(-1,1), y_t_pred.reshape(-1,1)]         # vector of predicted thrust on/off
    next_mini_batch = np.array([])

    sol = []

    t_initial = time_opt[2] # initial time
    t_final = time_opt[-1]  # final time

    time_int = np.linspace(t_initial, t_final, n_steps)     # vector of integration time
    time_int = np.reshape(time_int, [len(time_int), 1])

    for i in range(len(time_int)-1): # for i in range(60):
        t0 = time_int[i, 0]   # initial time of time step
        tf = time_int[i+1, 0] # final time of time step

        # dt = tf-t0
        # dxdt = Eom_fun(0, in_cond, param)
        # dm = dxdt[2]
        # print(dt)
        # print(dm)
        # deltaM = dt*dm
        # print(deltaM)
        # print(in_cond[2])
        # print(in_cond[2] + deltaM)
        # exit()

        x_int = ode(Eom_fun)
        x_int.set_integrator('dopri5', rtol=1e-20) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(in_cond, t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(param)
            x_int.integrate(tf, step=True)
            sol.append(x_int.y)

        sol = np.array(sol)

        x_new = sol[-1,:] # new state predicted
        aux = scaler.transform(x_new[:-1].reshape(1,-1))

        # build next batch
        next_mini_batch = np.r_[X_i[0,1,:n_features], X_i[0,2,:n_features], aux.reshape(n_features,)]
        next_mini_batch = next_mini_batch.reshape([1, mini_batch_size, n_features])

        # make new prediction
        prediction = model.predict(next_mini_batch)
        y_t_pred = prediction[0].argmax(axis=-1)

        X_i = next_mini_batch

        pred_X = np.r_[pred_X, x_new.reshape(1, 3)]
        pred_t = np.r_[pred_t, y_t_pred.reshape(1, 1)]

        in_cond = x_new # define new initial conditions
        param = [g0, alpha, T_max, T_min, y_t_pred]

        i = i+1
        next_mini_batch = np.array([])
        sol = []


    # put the vectors in the right form
    pred_t = np.array(pred_t)
    pred_t = np.reshape(pred_t, [pred_t.size, 1])

    tvec = np.r_[time_opt[:2].reshape(-1, 1), time_int.reshape(-1, 1)]

    predTrajectory = np.c_[pred_X, pred_t, tvec]
    return predTrajectory


if __name__ == "__main__":
    MODEL_PATH = '../model/LSTM.h5'
    TEST_DS_PATH = '../dataset/test_ds/ds_test_10.csv'
    PRED_TRAJ_PATH = '../dataset/test_ds/predictedTrajectories_LSTM'

    # print(util.GetDatasetDimension(TEST_DS_PATH, 200))
    # exit()

    PredictTestTrajectories(MODEL_PATH, TEST_DS_PATH, PRED_TRAJ_PATH, 100)
