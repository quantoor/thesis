clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 100;
tol = 1e-7;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');

aux = [];

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 80);

for i = 1:N
    % Random BC
    z0_rand = unifrnd(1000, 1500);
    vz0_rand = unifrnd(-30, -20);
    m0_vec = unifrnd(1100, 1400);
    BC = [z0_rand, vz0_rand, m0_vec];

    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc

    state = state(1:end-1,:);
    y = y(1:end-1);
    time = time(1:end-1);
    
    [Xfix, yfix, tfix] = GPOPS_SampleFix(state, y, time, 100);
    Mat_ds = [Xfix, yfix, tfix];
    
    aux = [aux; Mat_ds];
   
    
%     figure()
%     subplot(411)
%     plot(time, state(:,1),'.', tfix, Xfix(:,1),'o')
%     ylabel('Altitude [m]')    
%     subplot(412)
%     plot(time, state(:,2),'.', tfix, Xfix(:,2),'o')
%     ylabel('Velocity [m/s]')    
%     subplot(413)
%     plot(time, state(:,3),'.', tfix, Xfix(:,3),'o')
%     ylabel('Mass [kg]')    
%     subplot(414)
%     plot(time, y,'.', tfix, yfix(:,1),'o')
%     ylabel('Thrust [m/s^2]')
    ppm.increment()
end

%%
n_points = size(aux,1);

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);

