clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 500;
tol = 1e-6;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');

aux = [];

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 80);

for i = 1:N
    % Random BC
    z0_rand = unifrnd(1000, 1500);
    vz0_rand = unifrnd(-30, -20);
    m0_vec = unifrnd(1100, 1400);
    BC = [z0_rand, vz0_rand, m0_vec];

    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc

    Mat_ds = [state, y, time];
    Mat_ds = Mat_ds(1:end-1, :); % remove last state
    aux = [aux; Mat_ds];
    
%     figure()
%     subplot(411)
%     plot(time, state(:,1),'.')
%     ylabel('Altitude [m]')    
%     subplot(412)
%     plot(time, state(:,2),'.')
%     ylabel('Velocity [m/s]')    
%     subplot(413)
%     plot(time, state(:,3),'.')
%     ylabel('Mass [kg]')    
%     subplot(414)
%     plot(time, y,'.')
%     ylabel('Acceleration [m/s^2]')
    ppm.increment()
end

%%
n_points = size(aux,1);

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);

