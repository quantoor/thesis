import numpy as np
import pandas as pd
import keras
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import pickle
from scipy.integrate import ode
import scipy.optimize
from numpy.random import seed
from tensorflow import set_random_seed
import sklearn

import sys
sys.path.insert(0, '../../common1D')
import util, util_plot, Eom
from elm import GenELMRegressor
from random_layer import MLPRandomLayer, RBFRandomLayer
import argparse
from decimal import Decimal


def TrainNetwork(aux, FLAGS, overwrite=False):
    seed(42)
    set_random_seed(42)
    try:
        model = pickle.load(open(aux.MODEL, 'rb'))
        if not overwrite:
            print('Model already exists, exit training.\n')
            return
    except:
        pass

    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    L = train_ds.shape[0]
    split = int(0.85*L)

    X_train = train_ds.iloc[:split, :3].values    # state
    y_train = train_ds.iloc[:split, 3].values     # target

    X_val = train_ds.iloc[split:, :3].values    # state
    y_val = train_ds.iloc[split:, 3].values     # target

    # Input scaling
    scaler = MinMaxScaler(feature_range=(0,1))
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.transform(X_val)
    pickle.dump(scaler, open(aux.SCALER,'wb'))

    # =============================================================================
    # ELM
    # =============================================================================
    neurons_list = [100,300,500,800,900, 1000,1200,1400, 1500, 1600,1700,1800]
    # neurons_list = [1800,2000,2200,2500,3000]
    # neurons_list = [3000, 3200, 3500, 3800, 4000]
    old_loss = 1
    loss_train_vec = []
    loss_val_vec = []
    for n_neurons in neurons_list:

        srhl_rbf = RBFRandomLayer(n_hidden=n_neurons, rbf_width=0.1, random_state=42)
        model = GenELMRegressor(srhl_rbf)

        # Train
        print("Training with " + str(n_neurons) + " neurons...")
        model.fit(X_train, y_train.reshape(-1,1))

        # Train loss
        y_pred_train = model.predict(X_train)
        y_pred_train = (y_pred_train.flatten() > 0.5) * 1
        loss_train = sklearn.metrics.log_loss(y_train, y_pred_train)
        loss_train_vec.append(loss_train)

        # Validation loss
        y_pred_val = model.predict(X_val)
        y_pred_val = (y_pred_val.flatten() > 0.5) * 1
        loss_val = sklearn.metrics.log_loss(y_val, y_pred_val)
        loss_val_vec.append(loss_val)

        if loss_val < old_loss:
            best_model = model
            best_neurons = n_neurons
        old_loss = loss_val
        print('Train loss: %.2E' % Decimal(loss_train))
        print('Validation loss: %.2E' % Decimal(loss_val))
        print()

    util_plot.PlotTrainValLossELM(aux, loss_train_vec, loss_val_vec, neurons_list)

    print('Best number of neurons: ' + str(best_neurons))

    # Save best model
    best_model.hidden_activations_ = []
    best_model.hidden_layer.input_activations_ = []
    pickle.dump(best_model, open(aux.MODEL, 'wb'))
    print("Best model saved.")


def Main(FLAGS):
    # =============================================================================
    #  Main parameters
    # =============================================================================
    type = 'dagger'
    net = 'ELM'
    if FLAGS.iter>-1:
        iter = str(FLAGS.iter)
    else:
        iter = '0'
    dist = True
    start_point = 0
    d_bc = 2
    aux = util.Aux(type, net, iter, d_bc, dist, start_point=start_point)

    # =============================================================================
    #  Main
    # =============================================================================
    # TrainNetwork(aux, FLAGS, overwrite=True)
    # rmse = util.GPOPS_EvaluateNetwork(aux)

    util.GPOPS_MonteCarloSimulation(aux, overwrite=True)
    # util.GetResults(aux, rmse)

    # util.ZEM_ZEV_CollectStatesToCorrect(aux)
    # util.ZEM_ZEV_CollectBCForDAgger(aux, plot=0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--iter', type=int, default=-1, help='iteration')
    # parser.add_argument('--n_hidden', type=int, default=128, help='hidden neurons')
    # parser.add_argument('--hyper_mode', type=bool, default=False, help='If true, just compare different models')
    # parser.add_argument('--layers', type=int, default=4, help='hidden layers')
    # parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
    # parser.add_argument('--batch_size', type=int, default=8, help='mini batch size')
    # parser.add_argument('--epochs', type=int, default=500, help='epochs')
    # parser.add_argument('--l2', type=float, default=1e-6, help='L2 penalty regularization')
    # parser.add_argument('--custom_name', type=str, default=None, help='custom name for tensorboard folder')

    FLAGS = parser.parse_args()
    Main(FLAGS)
