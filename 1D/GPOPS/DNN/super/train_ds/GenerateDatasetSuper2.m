clear
close all
clc

ds_train_0 = csvread('ds_train_0.csv');
ds_train_new = csvread('ds_train_new.csv');
new_data_vec = [397, 42, 293, 255, 498, 115, 37];


for i = 1:length(new_data_vec)
    new_data = sum(new_data_vec(1:i));    
    
    ds_train_i = [ds_train_0; ds_train_new(1:new_data, :)];
    
    SAVE_PATH = strcat('ds_train_', num2str(i), '.csv');
    
    dlmwrite(SAVE_PATH, ds_train_i, 'delimiter', ',', 'precision', 8)
end