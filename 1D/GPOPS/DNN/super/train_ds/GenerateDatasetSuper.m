clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 30;
tol = 1e-7;

FILE_NAME = 'ds_train_new.csv';
TEMP_PATH = 'temp_aux.csv';

aux = [];
starting_idx = 1;

try
    aux = csvread(TEMP_PATH);
    starting_idx = size(aux,1)/n_points+1;
catch
    %
end

str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(0, str);
for i = starting_idx:N
    % Random BC
    z0_rand = unifrnd(1000, 1500);
    vz0_rand = unifrnd(-40, -20);
    m0_vec = unifrnd(1200, 1400);
    BC = [z0_rand, vz0_rand, m0_vec];

    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc

    Mat_ds = [state, y, time];
    Mat_ds = Mat_ds(1:end-1, :); % remove last state
    aux = [aux; Mat_ds];
    
    dlmwrite(TEMP_PATH, aux, 'delimiter', ',', 'precision', 8)
    
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
end
close(h)

%%
aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(FILE_NAME, aux, 'delimiter', ',', 'precision', 8)

