import numpy as np
import pandas as pd
import keras
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dense
from scipy.integrate import ode
import pickle
import datetime
from sklearn.preprocessing import MinMaxScaler
from mpl_toolkits.mplot3d import Axes3D
from numpy.random import seed
from tensorflow import set_random_seed
import argparse

import sys
sys.path.insert(0, '../../common1D')
import Eom, util, util_plot


def TrainNetwork(aux, FLAGS, overwrite=False):
    seed(42)
    set_random_seed(42)
    if not FLAGS.hyper_mode:
        try:
            load_model(aux.MODEL)
            if not overwrite:
                print('Model already exists, exit training.\n')
                return
        except:
            pass

    layers = FLAGS.layers
    n_hidden = FLAGS.n_hidden
    lr = FLAGS.lr
    batch_size = FLAGS.batch_size
    l2 = FLAGS.l2
    hyper_mode = FLAGS.hyper_mode
    epochs = FLAGS.epochs
    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    X_train = train_ds.iloc[:, :3].values    # state
    y_train = train_ds.iloc[:, 3].values     # thrust min/max

    y_train = keras.utils.to_categorical(y_train, 2) # label for classification

    scaler = MinMaxScaler(feature_range=(0,100))
    X_train = scaler.fit_transform(X_train)

    if not hyper_mode:
        pickle.dump(scaler, open(aux.SCALER, "wb"))

    # =============================================================================
    # DNN for classification and regression
    # =============================================================================
    initializer = keras.initializers.uniform()
    regularizer = keras.regularizers.l2(l2)

    input_layer = Input(shape=(3,), name='main_input')

    if layers == 2:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(input_layer)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
    elif layers == 3:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(input_layer)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
    elif layers == 4:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(input_layer)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)

    output_layer = Dense(2, activation='sigmoid', name='clas_output')(hidden_o)

    model = Model(inputs=[input_layer], outputs=[output_layer])
    # model.summary()

    # =============================================================================
    # Train model
    # =============================================================================
    optimizer = keras.optimizers.Adam(lr=lr, amsgrad=True)

    model.compile(loss={'clas_output': 'binary_crossentropy'},
        metrics={'clas_output':'accuracy'},
        optimizer=optimizer)

    call_backs = util.GetCallBacks(aux, FLAGS)

    history = model.fit(
        {'main_input': X_train},
        {'clas_output':y_train},
        validation_split=0.15, epochs=epochs, batch_size=batch_size, callbacks=call_backs)

    if not hyper_mode:
        pickle.dump(history, open(aux.TRAIN_HIST, "wb"))


def Main(FLAGS):
    # =============================================================================
    #  Main parameters
    # =============================================================================
    type = 'super'
    net = 'DNN'
    if FLAGS.iter>-1:
        iter = str(FLAGS.iter)
    else:
        iter = '0'
    d_bc = 2
    start_point = 0.85
    end_point = 0.94
    dist = None
    aux = util.Aux(type, net, iter, d_bc, dist, start_point=start_point, end_point=end_point)

    # =============================================================================
    #  Main
    # =============================================================================
    # TrainNetwork(aux, FLAGS, overwrite=True)
    acc = util.GPOPS_EvaluateNetwork(aux)
    # util_plot.PlotDecisionBoundary(aux)

    # util.GPOPS_MonteCarloSimulation(aux, overwrite=True)
    util.GetResults(aux, acc)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--iter', type=int, default=-1, help='iteration')
    parser.add_argument('--layers', type=int, default=3, help='hidden layers')
    parser.add_argument('--n_hidden', type=int, default=64, help='hidden neurons')
    parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
    parser.add_argument('--batch_size', type=int, default=8, help='mini batch size')
    parser.add_argument('--l2', type=float, default=1e-6, help='L2 penalty regularization')
    parser.add_argument('--epochs', type=int, default=300, help='epochs')
    parser.add_argument('--hyper_mode', type=bool, default=False, help='If true, just compare different models')
    parser.add_argument('--custom_name', type=str, default=None, help='custom name for tensorboard folder')

    FLAGS = parser.parse_args()
    Main(FLAGS)
