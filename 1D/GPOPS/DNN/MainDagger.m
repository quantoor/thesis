clear; close all; clc

dagger_it = 2;
tol = 1e-5;

ROOT = 'dagger/train_ds/';
TRAIN_OLD = strcat(ROOT, 'ds_train_', num2str(dagger_it), '.csv'); 
TRAIN_NEW = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.csv');
TEXT = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.txt');
BC4DAGGER = strcat(ROOT, 'bc4dagger_', num2str(dagger_it), '.csv');
NEW_TRAJ = strcat(ROOT, 'new_trajectories_', num2str(dagger_it), '.csv');
TEMP = 'aux_temp.csv';

BC_mat = csvread(BC4DAGGER);
old_train_ds = csvread(TRAIN_OLD);

skipped_trajectories = 0;


%% plot old train set and new states
figure()
hold on
plot3(old_train_ds(:,1),old_train_ds(:,2),old_train_ds(:,3),'b.')
plot3(BC_mat(:,1), BC_mat(:,2), BC_mat(:,3), 'r.','LineWidth',2)
legend('Train ds', 'bc4dagger')
grid on
xlabel('Position [m]')
ylabel('Velocity [m/s]')
zlabel('Mass [kg]')


%% ZEM ZEV DAgger
N = size(BC_mat, 1);
aux = [];
starting_idx = 1;

try
    aux = csvread(TEMP);
    starting_idx = size(aux,1)+1;
catch
    %
end
str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(0, str);

for i = starting_idx:N
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
    BC = BC_mat(i, :);
    disp(BC)
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = Mat_ds(1,:);
    catch
        skipped_trajectories = skipped_trajectories + 1;
        continue
    end

    if i>1
        aux = csvread(TEMP);
    end
    aux = [aux; Mat_ds];
    
    dlmwrite(TEMP, aux, 'delimiter', ',', 'precision', 8)
end
close(h)


%%
fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)
% aux = BC_mat;

aux(aux(:,end)>0, :) = [];
ds_augmented = [old_train_ds; aux];
ds_augmented_shuffled = randblock(ds_augmented, [1, size(aux,2)]); % shuffle

fprintf('\nOld training set: %d states\n', size(old_train_ds,1))
fprintf('New states: %d states\n', size(aux,1));
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))

logFileID = fopen(TEXT, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(old_train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fclose(logFileID);

dlmwrite(TRAIN_NEW, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)
dlmwrite(NEW_TRAJ, aux, 'delimiter', ',', 'precision', 8)
delete(TEMP)