clear
close all
clc

it = 4;

TRAIN_DS = strcat('dagger/train_ds/ds_train_', num2str(it), '.csv'); 
PREDICTED = strcat('dagger/predictions/predictedTrajectories_', num2str(it), '.csv');

ds = csvread(TRAIN_DS);
size(ds)

pred = csvread(PREDICTED);

figure
hold on

plot3(ds(:,1), ds(:,2), ds(:,3), 'b.')
plot3(pred(:,1), pred(:,2), pred(:,3), 'r.')

xlabel('Position [m]')
ylabel('Velocity [m/s]')
zlabel('Mass [kg]')
legend('Train set', 'Predicted trajectories')
grid on