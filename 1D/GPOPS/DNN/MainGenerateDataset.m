clear; close all; clc
rng('shuffle')
addpath('../../common1D/')

% Number of trajectories
N = 500;
tol = 1e-5;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');
TEMP_PATH = 'aux_temp.csv';

aux = [];

bc_train = load('../../common1D/common_bc/bc_train.mat');
bc_train = bc_train.bc_train;

try
    aux = csvread(TEMP_PATH);
    i = load('starting_idx.mat');
    starting_idx = i.i + 1;
    disp('Resuming...')
catch
    disp('Starting from beginning')
    starting_idx = 1;
end

str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(starting_idx, str);
for i = starting_idx:N
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
    
    % Random BC
    z0_rand = unifrnd(bc_train.z0_min, bc_train.z0_max);
    vz0_rand = unifrnd(bc_train.vz0_min, bc_train.vz0_max);
    m0_rand = unifrnd(bc_train.m0_min, bc_train.m0_max);
    BC = [z0_rand, vz0_rand, m0_rand];
    disp(BC)

    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc

    Mat_ds = [state, y, time];
    Mat_ds = Mat_ds(1:end-1, :); % remove last state
    aux = [aux; Mat_ds];
    
    dlmwrite(TEMP_PATH, aux, 'delimiter', ',', 'precision', 8)
    save('starting_idx', 'i')
    
%     figure()
%     subplot(411)
%     plot(time, state(:,1),'.')
%     ylabel('Altitude [m]')    
%     subplot(412)
%     plot(time, state(:,2),'.')
%     ylabel('Velocity [m/s]')    
%     subplot(413)
%     plot(time, state(:,3),'.')
%     ylabel('Mass [kg]')    
%     subplot(414)
%     plot(time, y,'.')
%     ylabel('Acceleration [m/s^2]')
%     return
end
close(h)

%%
n_points = size(aux,1);

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);

rmpath('../../common1D/')
delete(TEMP_PATH)
delete('starting_idx.mat')