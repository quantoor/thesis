clear; close all; clc
rng('shuffle')

dagger_it = 0;
tol = 1e-1;

BC2CORRECT_PATH = strcat('dagger/predictions/bc2correct_', num2str(dagger_it), '.csv');
CORRECTIONS_PATH = strcat('dagger/predictions/corrections_', num2str(dagger_it), '.csv');
TRAIN_PATH = strcat('dagger/train_ds/ds_train_', num2str(dagger_it), '.csv'); 

% BC_mat = csvread(BC2CORRECT_PATH);
train_ds = csvread(TRAIN_PATH);

% BC_mat = sortrows(BC_mat, 1); % order per ascending altitude

%% plot states to correct
figure()
hold on
for i = 1:size(train_ds,1)
    if train_ds(i, 4)
        plot3(train_ds(i,1),train_ds(i,2),train_ds(i,3),'r.')
    else
        plot3(train_ds(i,1),train_ds(i,2),train_ds(i,3),'b.')
    end
end
% plot_cube([1000, -40, 1200], 500, 20, 200, 'g')
% plot3(BC_mat(:,2), BC_mat(:,3), BC_mat(:,4), 'r.')
legend('Thrust max', 'Thrust min')
grid on
xlabel('Position [m]')
ylabel('Velocity [m/s]')
zlabel('Mass [kg]')
% view(3)
return

%% correct labels
N = size(BC_mat, 1);
aux = [];
aux2 = []; % to keep skipped trajectories
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 80);
for i = 1:N
    BC = BC_mat(i, :);
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC(2:end), tol);    
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = [BC(1), Mat_ds(1, :)];
    catch
        skipped_trajectories = skipped_trajectories + 1;
        aux2 = [aux2; BC(1)];
        continue
    end
    
    aux = [aux; Mat_ds];  
    
    ppm.increment()
end


%%
fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

if ~isempty(aux2) % if aux2 is non empty vector
    aux2 = [aux2, zeros(size(aux2,1), 5)];
    aux = [aux; aux2];
end

aux = sortrows(aux, 1);
aux = aux(:, 2:end); % remove first column

dlmwrite(CORRECTIONS_PATH, aux, 'delimiter', ',', 'precision', 8)



% 'delete(myCluster.Jobs)' to remove all jobs created with profile local.
% To create 'myCluster' use 'myCluster = parcluster('local')'