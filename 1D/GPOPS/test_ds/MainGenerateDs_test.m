clear; close all; clc
rng('shuffle')

iter = 6;
n_points = 100;
tol = 1e-2;

ROOT = '../../common1D/common_bc/bc_test_';

bc_test = csvread(strcat(ROOT, num2str(iter), '.csv'));
N = size(bc_test, 1);

SAVE_PATH = strcat('ds_test_', num2str(iter), '.csv');
TEMP_PATH = 'aux_temp.csv';

aux = [];
starting_idx = 1;

try
    aux = csvread(TEMP_PATH);
    starting_idx = size(aux,1)/n_points + 1;
catch
    %
end
% str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
% h = waitbar(0, str);
for i = starting_idx:N
    BC = bc_test(i, :);
    BC = [450,-40.5,950];

    % Compute trajectory
    tic
    [X, y, time] = GPOPS_1D(BC, tol);
    toc
    
    % Uniform sequence length and time step
    % NB THE THRUST IS WRONG BECOUSE OF INTERPOLATION
    [Xfix, yfix, tfix] = GPOPS_SampleFix(X, y, time, n_points);    
    Mat_ds = [Xfix, yfix, tfix];
    
    if i>1
        aux = csvread(TEMP_PATH);
    end
    aux = [aux; Mat_ds];
    
    dlmwrite(TEMP_PATH, aux, 'delimiter', ',', 'precision', 8)
    figure()
    subplot(411)
    plot(time, X(:,1),'-o')
    ylabel('Position [m]')    
    subplot(412)
    plot(time, X(:,2),'-o')
    ylabel('Velocity [m/s]')    
    subplot(413)
    plot(time, X(:,3),'-o')
    ylabel('Mass [kg]')    
    subplot(414)
    plot(time, y,'-o')
    ylabel('Output')
    xlabel('Time [s]')
    return
    
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
end
close(h)

dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)
delete(TEMP_PATH)