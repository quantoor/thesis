function PlotDecisionBoundary(aux)

% load model and scaler
model = load(aux.MODEL);
model = model.model;

scaler = load(aux.SCALER);
scaler = scaler.scaler;

N = 50;
m0 = 1300;
x_vec = linspace(0, 1500, N);
y_vec = linspace(-50, 0, N);

figure
title('Decision boundary')
hold on
grid on

for i = x_vec
    for j = y_vec
        Xi = [i, j, m0];
        y_pred = elm_predict(model, Xi./scaler);

        if y_pred
            plot(i, j, 'r.')
        else
            plot(i, j, 'b.')
        end
    end
end
xlabel('Position [m]')
ylabel('Velocity [m/s]')