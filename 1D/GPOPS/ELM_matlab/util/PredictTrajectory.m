function predictedTrajectory = PredictTrajectory(model, X_opt, t_max, scaler, dt)

landed = false;

% first input
X_i = X_opt(1, :);

% first prediction
y_pred = elm_predict(model, X_i./scaler);
state0 = X_i;

% % parameters
% g0 = 1.622;
% alpha = 5e-4;
% T_max = 3400;
% T_min = 1000;

% online integration
pred_state = state0;
pred_t = y_pred;

time_int = [0 : dt : t_max]';

for i = 1:length(time_int)-1
    t0 = time_int(i);
    tf = time_int(i+1);
    
    [~, Y] = ode23(@Eom, [t0, tf], state0, [], y_pred);
    
    X_new = Y(end, :);
    
    pred_state = [pred_state; X_new];
    
    y_pred = elm_predict(model, X_new./scaler);
    pred_t = [pred_t; y_pred];
    
    if X_new(1) < 0
        landed = true;
        break
    end
    
    if X_new(2) > 0
        break
    end
    
    state0 = X_new;    
end

predictedTrajectory = [pred_state, pred_t, time_int(1:i+1)];
end