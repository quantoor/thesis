function Train_ELM(aux)

train_ds = load(aux.TRAIN_DS);

n_hidden = aux.n_neurons;
activation = aux.activation;

%% Train
X_train = train_ds(:, 1:3);
scaler = max(abs(X_train));
X_train_norm = X_train./scaler; % normalize input data
% try to normalize like keras StandardScaler (subtract mean and divide by
% std deviation) ???

y_train = train_ds(:, 4);

disp('Training model...')
[model, ~, train_acc] = elm_train(X_train_norm, y_train, 1, n_hidden, activation);

fprintf('Train accuracy: %f\n', train_acc)

save(aux.MODEL, 'model');
save(aux.SCALER, 'scaler');

end