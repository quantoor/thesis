function MonteCarlo_ELM(aux)

% load test data
ds_opt = load(aux.TEST_DS);

% load train scaler
scaler = load(aux.SCALER);
scaler = scaler.scaler;

X_opt = ds_opt(:, 1:3);
time_opt = ds_opt(:, 5);
n_traj = size(ds_opt, 1)/aux.n_steps;


% load model
model = load(aux.MODEL);
model = model.model;



for i = 0:n_traj-1
    j = i*aux.n_steps;
    X = X_opt(j+1:j+aux.n_steps, :);
    t_max = time_opt(j+aux.n_steps);
    predictedTrajectory = PredictTrajectory(model, X, t_max, scaler, aux.dt);
    predictedTrajectories{i+1} = predictedTrajectory;    
    disp(predictedTrajectory(end, 1:2))
    
    fprintf('Trajectories predicted: %d/%d\n', i+1, n_traj)
end

save(aux.PRED_TRAJ, 'predictedTrajectories')


end
