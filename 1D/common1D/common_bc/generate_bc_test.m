clear; close all; clc
rng('shuffle')

N = 50; % number of trajectories
SAVE_PATH = 'bc_test_8.csv';

try
    csvread(SAVE_PATH);
    disp('bc_test already found, exit.')
    return
catch
    %
end

aux = [];

bc_train = load('bc_train.mat');
bc_train = bc_train.bc_train;

delta = 0.05;

z0_min = (1+delta) * bc_train.z0_min;
z0_max = (1-delta) * bc_train.z0_max;
vz0_min = (1-delta) * bc_train.vz0_min;
vz0_max = (1+delta) * bc_train.vz0_max;
m0_min = (1+delta) * bc_train.m0_min;
m0_max = (1-delta) * bc_train.m0_max;

fprintf('Position range: \t%d \t%d\n', z0_min, z0_max)
fprintf('Velocity range: \t%d \t%d\n', vz0_min, vz0_max)
fprintf('Mass range:     \t%d \t%d\n', m0_min, m0_max)

for i = 1:N
    % Random BC
    z0_rand = unifrnd(z0_min, z0_max);
    vz0_rand = unifrnd(vz0_min, vz0_max);
    m0_rand = unifrnd(m0_min, m0_max);
    BC = [z0_rand, vz0_rand, m0_rand];

    aux = [aux; BC];
end

dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)