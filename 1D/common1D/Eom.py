import numpy as np
import math
from scipy.optimize import fsolve


def Eom_GPOPS(t, x, y_pred):
    h, vh, m = x

     # Parameters
    g0  = 1.622
    # alphaM = 5*10**-4 <== fuuuck!! super hidden bug
    alphaM = 1/200/9.81 # 1 / Isp / g0 earth

    T = 1000 + y_pred * 2400

    # EoM
    hdot = vh;
    vhdot = -g0 + T/m;
    mdot = -alphaM*T;

    dxdt = [hdot, vhdot, mdot]

    return dxdt


def Eom_ZEM_ZEV(t, x, acc):
    h, vh, m = x

    # Parameters
    g0  = 1.622
    alphaM = 1/200/9.81

    if acc < 0: # avoid downward thrust
        acc = 0

    # EoM
    hdot = vh;
    vhdot = -g0 + acc;
    mdot = -m * acc * alphaM

    dxdt = [hdot, vhdot, mdot]
    return dxdt


def ComputeTgo(x, tgo):
    r, v = x

    if tgo < 3 and tgo > 1:
        XX0 = 3
    elif tgo < 1:
        XX0 = 1
    else:
        XX0 = 80

    tgo = fsolve(quartic, XX0, args=[r,v])
    return tgo


def quartic(T, *par):
    r, v = par[0]

    A = 1.622**2
    B = -2 * (v**2)
    C = 12 * (-r) * v
    D = -18 * r**2

    F = A*T**4 + B*T**2 + C*T + D;
    return F


# import pandas as pd
# ds = pd.read_csv('../ZEM_ZEV_tgo/test_ds/ds_test_0.csv', header=None)
# x_ds = ds.iloc[:,:2].values
# y_ds = ds.iloc[:,2].values
#
# print(ds.head())
#
# for i in range(len(x_ds)):
#     y_pred = ComputeTgo(x_ds[i,:], 1)
#     err = abs(y_pred-y_ds[i])
#     if err > 1e-3:
#         print(err)
