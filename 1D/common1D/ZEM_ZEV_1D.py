import numpy as np
import Eom
from scipy.integrate import odeint
from scipy.optimize import fsolve
import matplotlib.pyplot as plt

def ZEM_ZEV_3D(BC):
    g = 1.622
    dt = 0.05

    # initial conditions
    X0 = np.array([[0, 0, BC[0], 0, 0, BC[1]]]).T

    # find control to final point
    rf = np.array([[0, 0, 0]]).T
    vf = np.array([[0, 0, 0]]).T

    g = np.array([[0, 0, -g]]).T

    tInt = 10
    x0new = X0
    t = np.array([[0]])
    tgo = 1
    state = x0new.reshape(1,-1)
    T = [0]
    acc = np.empty([0,3])
    ControlAction = np.array([])
    Time = 0
    tgo_vec = []

    while tgo > 5e-2:
        x0 = x0new
        ti = t[-1]

        r = x0[:3].flatten()
        v = x0[3:].flatten()

        tgo = ComputeTgo(r, v, rf, vf, tgo)
        tgo_vec.append(tgo)

        # integrate without control
        a = np.array([[0,0,0]]).T
        tnc = np.arange(0, tgo, dt)

        sol = odeint(vertical, x0.flatten(), tnc, args=(g, a))
        x0nc = sol[-1, :]

        rnc = x0nc[:3]
        vnc = x0nc[3:]

        ZEM = rf - rnc.reshape(-1,1)
        ZEV = vf - vnc.reshape(-1,1)

        a = (6/tgo**2)*ZEM - (2/tgo)*ZEV

        acc = np.r_[acc, a.T]

        # integrate to next time with control
        sol = odeint(vertical, x0.flatten(), np.array([0,dt]), args=(g, a))
        x0new = sol[-1, :]

        state = np.r_[state, x0new.reshape(1,-1)]
        Time += dt
        T.append(Time)


    tgo_vec = np.array(tgo_vec)

    state = np.c_[state[:-1,[2,5]], tgo_vec]

    # 1D problem
    state = state[:-1, :]
    acc = acc[:-1, 2]
    T = np.array(T)[:-2]
    return state, acc, T


def ComputeTgo(r, v, rf, vf, tgo):
    g = np.array([[0, 0, -1.622]]).T

    A = np.dot(g.T, g)
    B = -2 * (np.dot(v.T,v) + np.dot(v.T,vf) + np.dot(vf.T,vf))
    C = 12 * np.dot((rf-r).T , (v+vf))
    D = -18 * np.dot((rf-r).T, (rf-r))

    if tgo < 3 and tgo > 1:
        XX0 = 3
    elif tgo < 1:
        XX0 = 1
    else:
        XX0 = 80

    tgo = fsolve(quartic, XX0, args=[A, B, C, D]);
    return tgo



def quartic(T, *par):
    A, B, C, D = par[0]
    F = A*T**4 + B*T**2 + C*T + D
    return F.flatten()


def vertical(X, t, *par):
    g = par[0]
    a = par[1]

    x = X[:3]
    xdot = X[3:6]
    xddot = g+a

    dxdt = np.c_[xdot.reshape(1,-1), xddot.reshape(1,-1)]

    return dxdt.flatten()


##########################################################################################
BC = [1000, -40]
state, acc, time = ZEM_ZEV_3D(BC)

print(state.shape)
print(acc.shape)
print(time.shape)


plt.figure()
plt.subplot(411)
plt.plot(state[:,0])
plt.ylabel('pos')

plt.subplot(412)
plt.plot(state[:,1])
plt.ylabel('vel')

plt.subplot(413)
plt.plot(state[:,2])
plt.ylabel('tgo')

plt.subplot(414)
plt.plot(acc)
plt.ylabel('acc')

plt.show()
