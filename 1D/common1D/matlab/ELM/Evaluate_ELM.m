function test_loss = Evaluate_ELM(aux)

test_ds = load(aux.TEST_DS);

%% Test
X_test = test_ds(:, 1:3);

scaler = load(aux.SCALER);
scaler = scaler.scaler;
X_test_norm = X_test./scaler;

y_test = test_ds(:, 4);

% load model
model = load(aux.MODEL);
model = model.model;

% test model
[~, ~, test_loss] = elm_predict_and_test(model, X_test_norm, y_test);

fprintf('Test loss: %f\n', test_loss)

PlotDecisionBoundary(aux)

end