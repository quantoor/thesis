function unrolledCell = UnrollCellArray(cell)

unrolledCell = [];

for i = 1:length(cell)    
    unrolledCell = [unrolledCell; cell{i}];    
end


end