function [output, TestingTime, TestingAccuracy] = elm_predict_and_test(model, X_test, y_test)

% Usage: elm_predict(TestingData_File)
% OR:    [TestingTime, TestingAccuracy] = elm_predict(TestingData_File)
%
% Input:
% TestingData_File      - Filename of testing data set
%
% Output: 
% TestingTime           - Time (seconds) spent on predicting ALL testing data
% TestingAccuracy       - Testing accuracy: 
%                           RMSE for regression or correct classification rate for classification
%
% MULTI-CLASSE CLASSIFICATION: NUMBER OF OUTPUT NEURONS WILL BE AUTOMATICALLY SET EQUAL TO NUMBER OF CLASSES
% FOR EXAMPLE, if there are 7 classes in all, there will have 7 output
% neurons; neuron 5 has the highest output means input belongs to 5-th class
%
% Sample1 regression: [TestingTime, TestingAccuracy] = elm_predict('sinc_test')
% Sample2 classification: elm_predict('diabetes_test')
%
    %%%%    Authors:    MR QIN-YU ZHU AND DR GUANG-BIN HUANG
    %%%%    NANYANG TECHNOLOGICAL UNIVERSITY, SINGAPORE
    %%%%    EMAIL:      EGBHUANG@NTU.EDU.SG; GBHUANG@IEEE.ORG
    %%%%    WEBSITE:    http://www.ntu.edu.sg/eee/icis/cv/egbhuang.htm
    %%%%    DATE:       APRIL 2004

%%%%%%%%%%% Macro definition
REGRESSION = 0;
CLASSIFIER = 1;

%%%%%%%%%%% Load testing dataset
% TV.T = test_data(:, 1)';
% TV.P = test_data(:, 2:size(test_data, 2))';

TV.P = X_test';
TV.T = y_test';
clear test_data;                                    %   Release raw testing data array

NumberofTestingData = size(TV.P,2);


if model.Elm_Type == CLASSIFIER

    %%%%%%%%%% Processing the targets of testing
    temp_TV_T = zeros(model.NumberofOutputNeurons, NumberofTestingData);
    for i = 1:NumberofTestingData
        for j = 1:size(model.label, 2)
            if model.label(1, j) == TV.T(1, i)
                break; 
            end
        end
        temp_TV_T(j, i) = 1;
    end
    TV.T = temp_TV_T*2-1;

end                                                 %   end if of Elm_Type

%%%%%%%%%%% Calculate the output of testing input
start_time_test = cputime;



tempH_test = model.InputWeight * TV.P;
clear TV.P;             %   Release input of testing data             
ind = ones(1,NumberofTestingData);
BiasMatrix = model.BiasofHiddenNeurons(:,ind);              %   Extend the bias matrix BiasofHiddenNeurons to match the demention of H
tempH_test = tempH_test + BiasMatrix;
switch lower(model.activation)
    case {'sig','sigmoid'}
        %%%%%%%% Sigmoid 
        H_test = 1 ./ (1 + exp(-tempH_test));
    case {'sin','sine'}
        %%%%%%%% Sine
        H_test = sin(tempH_test);        
    case {'hardlim'}
        %%%%%%%% Hard Limit
        H_test = hardlim(tempH_test);        
        %%%%%%%% More activation functions can be added here        
end
TY = (H_test' * model.OutputWeight)';                       %   TY: the actual output of the testing data
end_time_test = cputime;
TestingTime = end_time_test-start_time_test;          %   Calculate CPU time (seconds) spent by ELM predicting the whole testing data



if model.Elm_Type == REGRESSION
    TestingAccuracy = sqrt(mse(TV.T - TY));           %   Calculate testing accuracy (RMSE) for regression case
    output = TY;
end



if model.Elm_Type == CLASSIFIER
%%%%%%%%%% Calculate training & testing classification accuracy
    MissClassificationRate_Testing=0;

    for i = 1 : size(TV.T, 2)
        [~, label_index_expected]=max(TV.T(:,i));
        [~, label_index_actual]=max(TY(:,i));
        output(i) = model.label(label_index_actual);        
        if label_index_actual ~= label_index_expected
            MissClassificationRate_Testing = MissClassificationRate_Testing+1;
        end
    end
    TestingAccuracy = 1-MissClassificationRate_Testing/NumberofTestingData;  
end

end