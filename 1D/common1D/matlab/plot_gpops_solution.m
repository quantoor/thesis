function plot_gpops_solution(output)

%-------------------------------------------------------------------------%
%                             Extract Solution                            %
%-------------------------------------------------------------------------%
% Extract solution
solution = output.result.solution;
time = solution.phase(1).time;
state = solution.phase(1).state;
control = solution.phase(1).control;

% Find thrust components
thrustComponents=control(:,1:3);
gravityVector=[0,0,1];

% net force
netForce=thrustComponents.*control(:,4);

for i=1:length(output.meshhistory)
  mesh(i).meshPoints = [0 cumsum(output.meshhistory(i).result.setup.mesh.phase.fraction)];
  mesh(i).time =  output.meshhistory(i).result.solution.phase.time;
  mesh(i).iteration = i*ones(size(mesh(i).meshPoints));
  mesh(i).iterationTime = i*ones(size(mesh(i).time));
end

%-------------------------------------------------------------------------%
%                              Plot Solution                              %
%-------------------------------------------------------------------------%
figure(1);
plot(time,state(:,3),'-o');
xlabel('Time'); ylabel('Position (m)');
title('Position')
grid on

figure(2);
plot(time,state(:,6),'-o');
xlabel('Time'); ylabel('Velocity (m/s)');
title('Velocity')
grid on

figure(3);
plot(time,state(:,7),'-o');
xlabel('Time'); ylabel('Mass');
title('Mass')
grid on

figure(4);
plot(time,control(:,4),'-o');
xlabel('Time'); ylabel('Thrust');
title('Thrust')
grid on

figure(5);
tf = time(end);
for i=1:length(mesh)
  pp = plot(mesh(i).meshPoints*tf,mesh(i).iteration,'bo');
  set(pp,'LineWidth',1.25);
  hold on;
end
xl = xlabel('Mesh Point Location (Fraction of Interval)');
yl = ylabel('Mesh Iteration');
set(xl,'Fontsize',18);
set(yl,'Fontsize',18);
set(gca,'YTick',0:length(mesh),'FontSize',16,'FontName','Times');
grid on;
% print -dpng moonLanderMeshHistory.png

figure(6);
for i=1:length(mesh)
  pp = plot(mesh(i).time,mesh(i).iterationTime,'bo');
  set(pp,'LineWidth',1.25);
  hold on;
end
xl = xlabel('Collocation Point Location');
yl = ylabel('Mesh Iteration');
set(xl,'Fontsize',18);
set(yl,'Fontsize',18);
set(gca,'YTick',0:length(mesh),'FontSize',16,'FontName','Times');
grid on;

end