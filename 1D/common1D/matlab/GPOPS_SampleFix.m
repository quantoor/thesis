function [Xfix, ytfix, tfix] = GPOPS_SampleFix(X, y_t, tvect, n_steps)

tfix = linspace(tvect(1), tvect(end), n_steps); 
Xfix = zeros(n_steps, 3);                

[tvect, index] = unique(tvect); % remove doubles

for i = 1:size(X,2) % for each column of X
    col = X(:, i);
    Xfix(:, i) = interp1(tvect, col(index), tfix); % linear interpolation
end

ytfix = interp1(tvect, y_t(index), tfix);

ytfix = (ytfix>0.5)*1;

% output column vectors
ytfix = ytfix';
tfix = tfix';
end