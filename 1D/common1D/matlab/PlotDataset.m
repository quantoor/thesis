function PlotDataset(ds1, ds2)

figure()
hold on
plot3(ds1(1:2:end,1), ds1(1:2:end,2), ds1(1:2:end,4),'b.')
plot3(ds2(:,1), ds2(:,2), ds2(:,4),'r.')
xlabel('Position')
ylabel('Velocity')
zlabel('Acceleration')
grid on

end