function [state, y, time] = GPOPS_1D(BC, tol)

%----------------------- Lunar-Lander Problem -----------------------------%
%-------------------------------------------------------------------------%
    
iterations = 1;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.method          = 'hp-PattersonRao';
mesh.tolerance       = tol;
mesh.maxiterations   = 50;
mesh.colpointsmin    = 2;
mesh.colpointsmax    = 20;
mesh.phase.colpoints = 15; % initialize colpoints

% N = 10;
% mesh.phase.fraction   = ones(1,N)/N;
% mesh.phase.colpoints =  6*ones(1,N);
    
%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
auxdata.g = 1.622;
auxdata.Isp = 200;
auxdata.g0 = 9.81;
mDry = 500;
auxdata.phi = 0;

%-------------------------------------------------------------------------%
%----------------- Provide All Bounds for Problem ------------------------%
%-------------------------------------------------------------------------%
t0min = 0;   t0max = 0;
tfmin = 1;   tfmax = 100;

x0 = 0;         xf = 0;
y0 = 0;         yf = 0;
z0 = BC(1);     zf = 1e-5;

xdot0 = 0;      xdotf = 0;
ydot0 = 0;      ydotf = 0;
zdot0 = BC(2);  zdotf = 0;
m0 = BC(3);     mf = mDry; 

xmin = 0;        xmax =  0;
ymin = 0;        ymax =  0;
zmin = 0;        zmax =  z0;
xDotMin = 0;     xDotMax = 0;
yDotMin = 0;     yDotMax = 0;
zDotMin = -100;  zDotMax = 0;
mMin = mDry;     mMax = m0;

uxMin=0;   uxMax=0;
uyMin=0;   uyMax=0;
uzMin=0;   uzMax=1;
TFullThrottle = 4000;
Tmin = 0.25*TFullThrottle;   Tmax = 0.85*TFullThrottle;

%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%

% Time
bounds.phase.initialtime.lower = t0min;
bounds.phase.initialtime.upper = t0max;

bounds.phase.finaltime.lower = tfmin;
bounds.phase.finaltime.upper = tfmax;

% State
bounds.phase.initialstate.lower = [x0, y0, z0, xdot0, ydot0, zdot0, m0];
bounds.phase.initialstate.upper = [x0, y0, z0, xdot0, ydot0, zdot0, m0];

bounds.phase.state.lower = [xmin, ymin, zmin, xDotMin, yDotMin, zDotMin, mMin];
bounds.phase.state.upper = [xmax, ymax, zmax, xDotMax, yDotMax, zDotMax, mMax];

bounds.phase.finalstate.lower = [xf, yf, zf, xdotf, ydotf, zdotf, mDry];
bounds.phase.finalstate.upper = [xf, yf, zf, xdotf, ydotf, zdotf, m0];

% Control
bounds.phase.control.lower = [uxMin, uyMin, uzMin, Tmin];
bounds.phase.control.upper = [uxMax, uyMax, uzMax, Tmax];

% Path
bounds.phase.path.lower = 1;
bounds.phase.path.upper = 1;

% Integral
bounds.phase.integral.lower = 0;
bounds.phase.integral.upper = 1500000;

%%%%%%%%%%%%%%%%%%%%%%%%%%

it = 0;
for i = 1:iterations
    it = it+1;
    if i>1
        %-------------------------------------------------------------------------%
        %---------------------- Provide Guess of Solution ------------------------%
        %-------------------------------------------------------------------------%
        load('output.mat') 
        mesh.phase.colpoints = output.meshColPoints+10;     % add 10 coll points
        guess.phase.state    = output.result.solution.phase(1).state;
        guess.phase.control  = output.result.solution.phase(1).control;
        guess.phase.time     = output.result.solution.phase(1).time;
        guess.phase.integral = output.result.solution.phase(1).integral;
    else 
        %-------------------------------------------------------------------------%
        %---------------------- Provide Guess of Solution ------------------------%
        %-------------------------------------------------------------------------%
        tGuess               = [t0min; 70];
        xGuess               = [x0; xf];
        yGuess               = [y0; yf];
        zGuess               = [z0; zf];
        xDotGuess            = [xdot0; xdotf];
        yDotGuess            = [ydot0; ydotf];
        zDotGuess            = [zdot0; zdotf];
        mGuess               = [m0; mf];
        uxGuess              = [uxMin; uxMax];
        uyGuess              = [uyMin; uyMax];
        uzGuess              = [uzMin; uzMax];
        TGuess               = [Tmin; Tmax];
        guess.phase.state    = [xGuess , yGuess , zGuess, xDotGuess, yDotGuess, zDotGuess , mGuess];
        guess.phase.control  = [uxGuess , uyGuess , uzGuess , TGuess];
        guess.phase.time     = tGuess;
        guess.phase.integral = 700000;
    end
    
    %-------------------------------------------------------------------------%
    %------------- Assemble Information into Problem Structure ---------------%        
    %-------------------------------------------------------------------------%
    
    % Required fields    
    setup.name                        = 'Soft-Moon-Landing';
    setup.functions.endpoint          = @moonLanderEndpoint;
    setup.functions.continuous        = @moonLanderContinuous;
    setup.bounds                      = bounds;
    setup.guess                       = guess;
    
    % Optional fields
    setup.auxdata                     = auxdata;
    setup.derivatives.supplier        = 'sparseCD';
    setup.derivatives.derivativelevel = 'second';
    setup.method                      = 'RPM-Differentiation';
    setup.mesh                        = mesh;
    setup.nlp.solver                  = 'ipopt';
    setup.displaylevel                = 0; 
    
    %-------------------------------------------------------------------------%
    %----------------------- Solve Problem Using GPOPS2 ----------------------%
    %-------------------------------------------------------------------------%
    output = gpops2(setup);
    
    output.meshColPoints=mesh.phase.colpoints; % keep track of col points
    output.iterations=it;
    save('output.mat')
end

    % Extract solution
    solution = output.result.solution;
    time = solution.phase(1).time;
    state = solution.phase(1).state;
    control = solution.phase(1).control;
    
    % 1D problem
    state = state(:,[3,6,7]);
    y = control(:,4);
    
%     y = (y-1000)/2400;
    
    % remove middle point
    state(y>1000 & y<3400, :) = [];
    time(y>1000 & y<3400) = [];
    y(y>1000 & y<3400) = [];
    
%     Thrust is either 0 or 1
    lim = (1000+3400)/2;
    y = (y>lim)*1;

end