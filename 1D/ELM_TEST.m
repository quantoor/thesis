clear
clc

%% Dataset
X_train = linspace(0,2*pi,1000)';
y_r_train = sin(X_train);
y_c_train = (y_r_train >= 0)*1;
y_train = [y_r_train, y_c_train];

figure
grid on
hold on
plot(X_train, y_r_train,'b.')
plot(X_train, y_c_train, 'r.')


%% Train
n_hidden = 10000;
activation = 'hardlim';

scaler = max(abs(X_train));
X_train_norm = X_train./scaler; % normalize input data

[model, ~, train_acc] = elm_train(X_train_norm, y_train, 0, n_hidden, activation);

fprintf('Train accuracy: %f\n', train_acc)


%% Test
X_test = linspace(0,2*pi,300)';
y_r_test = sin(X_test);
y_c_test = (y_r_test >= 0)*1;
y_test = [y_r_test, y_c_test];

X_test_norm = X_test./scaler;

y_pred = elm_predict(model, X_test_norm);


close all
figure

hold on
subplot(211)
plot(X_test, y_r_test,'b.')
plot(X_test, y_pred(1,:), 'r.')
grid on

subplot(212)
plot(X_test, y_c_test,'b.')
plot(X_test, y_pred(2,:), 'r.')
grid on