clear 
close all


aux.iter = 0;

iter = num2str(aux.iter);

aux.TRAIN_DS = strcat('dagger\train_ds\ds_train_', iter, '.csv');
aux.SAVE_BC = strcat('dagger\train_ds\bc4dagger_', iter, '.csv');

aux.TEST_DS = strcat('..\test_ds\ds_test_', iter, '.csv');

aux.PRED_TRAJ = strcat('dagger\predictions\predictedTrajectories_', iter);
aux.BC2CORRECT = strcat('dagger\predictions\bc2correct_', iter, '.csv');
aux.CORRECTIONS = strcat('dagger\predictions\corrections_', iter, '.csv');

aux.MODEL = strcat('dagger\model\ELM_', iter, '.mat');
aux.SCALER = strcat('dagger\model\scaler_', iter, '.mat');

aux.TEXT_FILE =  strcat('dagger\results_', iter, '.txt');
aux.FIG_ROOT = strcat('dagger\img\fig_', iter, '_');

aux.n_steps = 100;
aux.dt = 0.1;
aux.SR_criteria = [7.5, 0.5];
aux.d_bc = 10;
aux.n_hidden = 500;


%%
% Train_ELM(aux, 0)
val_loss = Evaluate_ELM(aux);

MonteCarlo_ELM(aux)
GetResults(aux, val_loss);

ZEM_ZEV_CollectStatesToCorrect(aux)
% ZEM_ZEV_CollectBCForDagger(aux, 0)


