clear
close all
clc

dagger_it = 0;

ROOT = 'dagger/train_ds/';
TRAIN_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it), '.csv'); 
SAVE_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.csv');
TXT_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.txt');
BC_PATH = strcat(ROOT, 'bc4dagger_', num2str(dagger_it), '.csv');
NEW_TRAJ_PATH = strcat(ROOT, 'new_trajectories_', num2str(dagger_it+1), '.csv');

BC_mat = csvread(BC_PATH);
old_train_ds = csvread(TRAIN_PATH);

%% plot old train set and new states
figure()
hold on
scatter(old_train_ds(:,1),old_train_ds(:,2),'b.')
plot(BC_mat(:,1),BC_mat(:,2),'r.')
grid on


%% ZEM ZEV DAgger
N = size(BC_mat, 1);

aux = [];
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 60);

parfor i = 1:N
    BC = BC_mat(i, :);
    
    % Compute trajectory
    try
        tic
        [state, acc, time] = ZEM_ZEV_1D(BC);    
        toc
    catch
        disp('Trajectory skipped')
        skipped_trajectories = skipped_trajectories + 1;
        continue
    end
      
    % Preprocess data        
    Mat_ds = [state, acc, time];
    Mat_ds = Mat_ds(1:end-2, :); % remove last state   

    aux = [aux; Mat_ds]; 
    
    ppm.increment()
end

%%

ds_augmented = [old_train_ds; aux];
% ds_augmented(ds_augmented(:,1)<0, :) = []; % remove points with negative altitude
ds_augmented_shuffled = randblock(ds_augmented, [1, size(aux,2)]); % shuffle

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)
fprintf('\nOld training set: %d states\n', size(old_train_ds,1))
fprintf('New states: %d states\n', size(aux,1));
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))
fprintf('Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1))

logFileID = fopen(TXT_PATH, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(old_train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Skipped trajectories: %d\n', skipped_trajectories);
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fprintf(logFileID, 'Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1));
fclose(logFileID);

dlmwrite(SAVE_PATH, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)
dlmwrite(NEW_TRAJ_PATH, aux, 'delimiter', ',', 'precision', 8)


%%
close all
PlotDataset(old_train_ds, aux)