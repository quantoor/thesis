function Train_ELM(aux, type)

train_ds = load(aux.TRAIN_DS);

n_hidden = aux.n_hidden;
activation = 'sig';

%% Train
X_train = train_ds(:, 1:3);
scaler = max(abs(X_train));
X_train_norm = X_train./scaler; % normalize input data

y_train = train_ds(:, 4);

disp('Training model...')
[model, ~, train_loss] = elm_train([y_train, X_train_norm], type, n_hidden, activation);

if type
    fprintf('Train accuracy: %f\n', train_loss)
else
    fprintf('Train loss: %f\n', train_loss)
end

save(aux.MODEL, 'model');
save(aux.SCALER, 'scaler');

end