function predictedTrajectory = PredictTrajectory(model, X_opt, t_max, scaler, dt)

% first input
r = [0; 0; X_opt(1, 1)];
v = [0; 0; X_opt(1, 2)];
tgo = ComputeTgo(r, v, 1);

X_i = [X_opt(1,:), tgo];

% first prediction
y_pred = elm_predict(model, X_i./scaler);
state0 = X_opt(1,:);

% online integration
pred_state = X_i;
pred_t = y_pred;

time_int = [0 : dt : t_max]';

for i = 1:length(time_int)-1
    t0 = time_int(i);
    tf = time_int(i+1);
    
    [~, Y] = ode23(@Eom_ZEM_ZEV, [t0, tf], state0, [], y_pred);
    
    
    X_new = Y(end, :);  
    
    r = [0; 0; X_new(1)];
    v = [0; 0; X_new(2)];
    tgo = ComputeTgo(r, v, tgo);
    
      
    pred_state = [pred_state; [X_new,tgo]];
    
    y_pred = elm_predict(model, [X_new,tgo]./scaler);
    pred_t = [pred_t; y_pred];
    
    if X_new(1) < 0
        break
    end
    
    if X_new(2) > 0
        break
    end
    
    state0 = X_new;    
end

predictedTrajectory = [pred_state, pred_t, time_int(1:i+1)];
end