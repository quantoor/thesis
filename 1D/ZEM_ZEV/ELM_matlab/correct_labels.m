clear
close all
clc

dagger_it = 0;

BC2CORRECT_PATH = strcat('dagger/predictions/bc2correct_', num2str(dagger_it), '.csv');
CORRECTIONS_PATH = strcat('dagger/predictions/corrections_', num2str(dagger_it), '.csv');

BC_mat = csvread(BC2CORRECT_PATH);

%% plot states to correct
figure()
hold on
plot(BC_mat(:,2),BC_mat(:,3),'r.')
grid on


%% correct labels
N = size(BC_mat, 1);

aux = [];
aux2 = []; % to keep skipped trajectories
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    BC = BC_mat(i, :);
    
    % Compute trajectory
    try
        tic
        [state, acc, time] = ZEM_ZEV_1D(BC(2:end));    
        toc
    catch
        disp('Trajectory skipped')
        skipped_trajectories = skipped_trajectories + 1;
        aux2 = [aux2; BC(1)];
        continue
    end
%     'e i miei capezzolini piccini picci� se ne vanno in Francia'
       
    Mat_ds = [state, acc, time];
    Mat_ds = [BC(1), Mat_ds(1, :)];
    
    aux = [aux; Mat_ds];
    
    ppm.increment()
end

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

%%

if ~isempty(aux2) % if aux2 is non empty vector
    aux2 = [aux2, zeros(size(aux2,1), 5)];
    aux = [aux; aux2];
end

aux = sortrows(aux,1);

aux = aux(:, 2:end); % remove first column
dlmwrite(CORRECTIONS_PATH, aux, 'delimiter', ',', 'precision', 8)

