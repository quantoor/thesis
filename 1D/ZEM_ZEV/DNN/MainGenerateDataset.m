clear; close all; clc
rng('shuffle')
addpath('../../common1D/')
addpath('../../common1D/matlab')

% Number of trajectories
N = 500;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');

aux = [];
skipped_trajectories = 0;

bc_train = load('../../common1D/common_bc/bc_train.mat');
bc_train = bc_train.bc_train;

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    % Random BC
    z0_rand = unifrnd(bc_train.z0_min, bc_train.z0_max);
    vz0_rand = unifrnd(bc_train.vz0_min, bc_train.vz0_max);
    m0_rand = unifrnd(bc_train.m0_min, bc_train.m0_max);
    BC = [z0_rand, vz0_rand, m0_rand];

    % Compute trajectory
    tic
    [state, acc, time] = ZEM_ZEV_1D(BC);    
    toc

    % Preprocess data       
    Mat_ds = [state, acc, time];
    Mat_ds = Mat_ds(1:end-2,:); % remove last states 

    aux = [aux; Mat_ds];

%     figure()
%     subplot(411)
%     plot(time, state(:,1), tfix, Xfix(:,1),'.')
%     ylabel('Altitude [m]')
%     subplot(412)
%     plot(time, state(:,2), tfix, Xfix(:,2),'.')
%     ylabel('Velocity [m/s]')
%     subplot(413)
%     plot(time, state(:,3), tfix, Xfix(:,3),'.')
%     ylabel('Mass [kg]')
%     subplot(414)
%     plot(time, acc, tfix, yfix,'.')
%     ylabel('Acceleration [m/s^2]')
    ppm.increment()
end

n_points = size(aux,1);

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);

rmpath('../../common1D/')
rmpath('../../common1D/matlab')

%% plot 3d
close all
x = aux(:,1);
y = aux(:,2);
z = aux(:,5);
figure()
plot3(x, y, z, '.')
grid on
xlabel('x')
ylabel('y')
zlabel('z')