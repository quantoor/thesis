Dagger iteration: 2

Train set size: 21675 points
Test loss:		8.50E-03

Monte Carlo simulation:
Success rate:			76.00 %
Average final state:		[0.03, 0.16]
Distance CM:			4.91E-05