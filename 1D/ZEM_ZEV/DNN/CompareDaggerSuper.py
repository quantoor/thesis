import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../../common1D')
import Eom, util, util_plot

def Compare(n_iter):
    font = {'size'   : 16}
    import matplotlib
    matplotlib.rc('font', **font)

    dagger_results = np.zeros((n_iter+1, 5))
    super_results = np.zeros((n_iter+1, 5))
    x = []
    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')
        super_results[i, :] = np.load('super/results_' + str(i) + '.npy')

        TRAIN_PATH = 'dagger/train_ds/ds_train_' + str(i) + '.csv'
        temp = pd.read_csv(TRAIN_PATH, header=None)
        x.append(temp.shape[0])

    # fig = plt.figure(num=None, figsize=(10, 7))
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(nrows=4, sharex=True, figsize=(10, 7))

    ax1.plot(x, dagger_results[:,0], '-o')
    ax1.plot(x, super_results[:,0], '-o')
    # ax1.set_ylabel('Val. loss')

    ax2.plot(x, dagger_results[:,2], '-o')
    ax2.plot(x, super_results[:,2], '-o')
    # ax2.set_ylabel('Pos. [m]')

    ax3.plot(x, dagger_results[:,3], '-o')
    ax3.plot(x, super_results[:,3], '-o')
    # ax3.set_ylabel('Vel. [m/s]')

    ax4.plot(x, dagger_results[:,4], '-o', label='Dagger')
    ax4.plot(x, super_results[:,4], '-o', label='Supervised')
    # ax4.set_ylabel('D_CM')
    # ax4.set_xlabel('Train set size')

    # fig.legend(bbox_to_anchor=(0, 1), loc='upper left', ncol=1)

    fig.savefig('results_dagger_vs_super.png')
    plt.show()


def OnlyDagger(n_iter):
    dagger_results = np.zeros((n_iter+1, 5))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')

    x = np.linspace(0, n_iter, n_iter+1)

    fig = plt.figure(num=None, figsize=(10, 7))
    plt.subplot(411)
    plt.grid()
    plt.plot(x, dagger_results[:,0], '-o')
    plt.ylabel('Validation loss')

    plt.subplot(412)
    plt.grid()
    plt.plot(x, dagger_results[:,2], '-o')
    plt.ylabel('Final position [m]')

    plt.subplot(413)
    plt.grid()
    plt.plot(x, dagger_results[:,3], '-o')
    plt.ylabel('Final velocity [m/s]')

    plt.subplot(414)
    plt.grid()
    plt.plot(x, dagger_results[:,4], '-o')
    plt.ylabel('Von Mises Distance')
    plt.xlabel('Dagger iteration')

    fig.savefig('dagger/img/dagger_results.png')
    plt.show()


def CreateTable(n_iter, train_data_vec):
    dagger_results = np.zeros((n_iter+1, 5))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')
    dagger_results = np.c_[np.arange(0,n_iter+1,1), dagger_results, train_data_vec]

    df = pd.DataFrame(dagger_results)
    df.columns = ['Dagger iter','Val loss','SR','Final Pos [m]','Final Vel [m/s]','D_CM','N train data']
    df = df.drop(['SR'], axis=1)
    # pd.set_option('display.float_format', lambda x: '%.1f' % x)
    # pd.options.display.float_format = '{:.2E}'.format
    df['Dagger iter'] = df['Dagger iter'].round(0)
    df['Final Pos [m]'] = df['Final Pos [m]'].round(2)
    df['Final Vel [m/s]'] = df['Final Vel [m/s]'].round(2)

    fig, ax = util_plot.render_mpl_table(df, col_width=4.0)
    fig.savefig('dagger/img/dagger_results_tab.png')
    print(df)
    return


if __name__ == "__main__":
    it = 4

    train_data_vec = []
    for i in range(it+1):
        TRAIN_PATH = 'dagger/train_ds/ds_train_' + str(i) + '.csv'
        temp = pd.read_csv(TRAIN_PATH, header=None)
        train_data_vec.append(temp.shape[0])

    # OnlyDagger(it)
    Compare(it)
    # CreateTable(it, train_data_vec)
