import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt
import pickle
import time
import argparse
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dense
from sklearn.preprocessing import MinMaxScaler
from numpy.random import seed
from tensorflow import set_random_seed

import sys
sys.path.insert(0, '../../common1D')
import util, util_plot

import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin'


def TrainNetwork(aux, FLAGS, overwrite=False):
    seed(42)
    set_random_seed(42)
    if not FLAGS.hyper_mode:
        try:
            load_model(aux.MODEL)
            if not overwrite:
                print('Model already exists, exit training.\n')
                return
        except:
            pass

    layers = FLAGS.layers
    n_hidden = FLAGS.n_hidden
    lr = FLAGS.lr
    batch_size = FLAGS.batch_size
    l2 = FLAGS.l2
    epochs = FLAGS.epochs
    hyper_mode = FLAGS.hyper_mode
    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    X_train = train_ds.iloc[:, :3].values    # state
    y_train = train_ds.iloc[:, 4].values     # target

    scaler = MinMaxScaler(feature_range=(0,100))
    X_train = scaler.fit_transform(X_train)

    if not hyper_mode:
        pickle.dump(scaler, open(aux.SCALER, 'wb'))

    # =============================================================================
    # DNN
    # =============================================================================
    initializer = keras.initializers.uniform()
    regularizer = keras.regularizers.l2(l2)

    visible = Input(shape=(3,), name='input_layer')

    if layers == 2:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
    elif layers == 4:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)
    elif layers == 6:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_4 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)
        hidden_5 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_4)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_5)

    output_layer = Dense(1, activation='relu', name='output_layer')(hidden_o)
    model = Model(inputs=[visible], outputs=[output_layer])

    # =============================================================================
    # Train model
    # =============================================================================
    optimizer = keras.optimizers.Adam(lr=lr, amsgrad=True)

    model.compile(
        loss={'output_layer': "mean_squared_error"},
        metrics={'output_layer':'mse'},
        optimizer=optimizer)

    call_backs = util.GetCallBacks(aux, FLAGS)

    history = model.fit(
        {'input_layer': X_train},
        {'output_layer': y_train},
        validation_split=0.15, epochs=epochs, batch_size=batch_size,
        verbose=2, callbacks=call_backs)

    if not hyper_mode:
        pickle.dump(history, open(aux.TRAIN_HIST, "wb"))


def Main(FLAGS):
    # =============================================================================
    #  Main parameters
    # =============================================================================
    type = 'dagger'
    net = 'DNN'
    if FLAGS.iter>-1:
        iter = str(FLAGS.iter)
    else:
        iter = '4'
    dist = False
    start_point = 0.7   # 0.9
    d_bc = 5            # 1
    aux = util.Aux(type, net, iter, d_bc, dist, start_point=start_point)

    # =============================================================================
    #  Main
    # =============================================================================
    # TrainNetwork(aux, FLAGS, overwrite=True)
    rmse = util.ZEM_ZEV_EvaluateNetwork(aux)

    # util.ZEM_ZEV_MonteCarloSimulation(aux, overwrite=True)
    util.GetResults(aux, rmse)

    # util.ZEM_ZEV_CollectStatesToCorrect(aux)
    # util.ZEM_ZEV_CollectBCForDAgger(aux, plot=1)


    # model = load_model(aux.MODEL)
    # print(model.summary())
    # keras.utils.plot_model(model, to_file=aux.MODEL[:-2]+'png')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--iter', type=int, default=-1, help='iteration')
    parser.add_argument('--layers', type=int, default=4, help='hidden layers')
    parser.add_argument('--n_hidden', type=int, default=128, help='hidden neurons')
    parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
    parser.add_argument('--batch_size', type=int, default=8, help='mini batch size')
    parser.add_argument('--epochs', type=int, default=500, help='epochs')
    parser.add_argument('--l2', type=float, default=1e-6, help='L2 penalty regularization')
    parser.add_argument('--hyper_mode', type=bool, default=False, help='If true, just compare different models')
    parser.add_argument('--custom_name', type=str, default=None, help='custom name for tensorboard folder')

    FLAGS = parser.parse_args()
    Main(FLAGS)
