Dagger iteration: 4

Train set size: 22628 points
Test loss:		1.07E-02

Monte Carlo simulation:
Success rate:			50.00 %
Average final state:		[0.46, 1.13]
Distance CM:			1.26E-04