Dagger iteration: 2

Train set size: 10505 points
Test loss:		3.83E-03

Monte Carlo simulation:
Success rate:			100.00 %
Average final state:		[0.00, 0.07]
Distance CM:			3.17E-04