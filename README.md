# Workflow
- Generate training dataset (4000 trajectories) with Gpops
- Generate validating dataset (500 trajectories) with Gpops
- Train network on training dataset
- Run Monte Carlo on test dataset (TODO, for now it is used the same validating dataset)
- Analyze predicted trajectories looking at: class accuracy, regr RMSE, final state
- Collect BC for DAgger from the worst trajectories
- Generate new trajectories with Gpops
- Augment training dataset with new trajectories (shuffle trajectories)
- Train the network on augmented dataset
- Analyze the state distribution, with old policy and DAgger policy
- Repeat DAgger untile the state ditributions match

# Notes
- Unexpectedly, removing from the training set the trajectories too similar to the validating set, has greatly improved the training

# TODO
- Plot metrics to show improvement due to DAgger iteration
- Plot state distribution as a difference between training set and predicted trajectories
