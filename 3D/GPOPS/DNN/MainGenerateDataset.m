clear; close all; clc
rng('shuffle')
addpath('../../common3D/matlab')

% Number of trajectories
N = 1;
tol = 1e-5;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');

aux = [];

% try
%     parpool
% catch
%     %
% end
% str = strcat('Generating ', num2str(N), ' trajectories... ');
% ppm = ParforProgMon(str, N, 1, 500, 60);
for i = 1:N
    % Random BC
    x0_rand = unifrnd(1500, 2000);
    y0_rand = unifrnd(-100, 100);
    z0_rand = unifrnd(1000, 1500);
    vx0_rand = unifrnd(-60, -50);
    vy0_rand = unifrnd(-10, 10);
    vz0_rand = unifrnd(-30, -20);
    m0_rand = unifrnd(1200, 1400);
    BC = [x0_rand, y0_rand, z0_rand, vx0_rand, vy0_rand, vz0_rand, m0_rand];
%     BC = [1500, 0, 1000, -60, -10, -30, 1400];
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_3D(BC, tol);
    toc

    Mat_ds = [state, y, time];
    state(end, :)
    length(time)
    time(end)
%     Mat_ds = Mat_ds(1:end-1, :); % remove last state

    aux = [aux; Mat_ds];
    
%     figure()
%     subplot(411)
%     plot(time, state(:,1:3),'.')
%     ylabel('Position [m]')    
%     subplot(412)
%     plot(time, state(:,4:6),'.')
%     ylabel('Velocity [m/s]')    
%     subplot(413)
%     plot(time, state(:,7),'.')
%     ylabel('Mass [kg]')    
%     subplot(414)
%     hold on
%     plot(time, y(:,1:3),'.')
%     plot(time, y(:,4),'.')
%     ylabel('Acceleration [m/s^2]')
%     return
%     ppm.increment()
end

%%

n_points = size(aux,1);

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);
rmpath('../../common3D/matlab')
