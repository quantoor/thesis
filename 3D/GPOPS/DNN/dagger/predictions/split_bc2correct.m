clear; close all; clc
rng('shuffle')

dagger_it = 0;

BC2CORRECT = strcat('dagger/predictions/bc2correct_', num2str(dagger_it), '.csv');
OLD_TRAIN_DS = strcat('../train_ds/ds_train_', num2str(dagger_it), '.csv'); 

BC_mat = csvread(BC2CORRECT);
old_train_ds = csvread(OLD_TRAIN_DS);

idx2remove = BC_mat(:, 3) <= 0;
BC_mat(idx2remove, :) = [];
BC_mat = sortrows(BC_mat, 3); % order per descending altitude

%% split dataset
idx = round(size(BC_mat,1)/5);

BC_mat_1 = BC_mat(1:idx, :);
BC_mat_2 = BC_mat(idx+1:2*idx, :);
BC_mat_3 = BC_mat(2*idx+1:3*idx, :);
BC_mat_4 = BC_mat(3*idx+1:4*idx, :);
BC_mat_5 = BC_mat(4*idx+1:end, :);

size(BC_mat_1)
size(BC_mat_2)
size(BC_mat_3)
size(BC_mat_4)
size(BC_mat_5)

% dlmwrite('bc2correct_ord_1.csv', BC_mat_1, 'delimiter', ',', 'precision', 8)
% dlmwrite('bc2correct_ord_2.csv', BC_mat_2, 'delimiter', ',', 'precision', 8)
% dlmwrite('bc2correct_ord_3.csv', BC_mat_3, 'delimiter', ',', 'precision', 8)
% dlmwrite('bc2correct_ord_4.csv', BC_mat_4, 'delimiter', ',', 'precision', 8)
% dlmwrite('bc2correct_ord_5.csv', BC_mat_5, 'delimiter', ',', 'precision', 8)


%% plot states to correct
figure()
hold on
plot3(old_train_ds(:,1),old_train_ds(:,2),old_train_ds(:,3),'b.')
% plot3(BC_mat(:,1), BC_mat(:,2), BC_mat(:,3),'r.')
grid on
axis equal
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
xlim([0, 2000])
ylim([-200, 200])
zlim([0, 1500])
% title(strcat('Train dataset, iter ', num2str(dagger_it)))
plot_cube([1500, -100, 1000], 500, 200, 500, 'r')
view(3)
