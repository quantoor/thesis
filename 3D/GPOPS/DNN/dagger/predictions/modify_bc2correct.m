clear 
clc

FILE = 'bc2correct_ord_3.csv';

bc = csvread(FILE);
bc(:,6) = bc(:,6) + 0.1;

dlmwrite(FILE, bc, 'delimiter', ',', 'precision', 8)