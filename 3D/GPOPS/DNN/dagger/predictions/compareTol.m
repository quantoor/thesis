clc
clear
close all

% corrections1e_5 = csvread('corrections_1e-5.csv');
% corrections1e_6 = csvread('corrections_1e-6.csv');
% corrections1e_7 = csvread('corrections_1e-7.csv');

% y1e_5 = corrections1e_5(:, 8:10);
% y1e_6 = corrections1e_6(:, 8:10);
% y1e_7 = corrections1e_7(:, 8:10);

% sum(abs(y1e_5 - y1e_6))
% sum(abs(y1e_6 - y1e_7))
% sum(abs(y1e_5 - y1e_7))
% return

bc2correct = csvread('bc2correct_1.csv');

tol = 1e-6;
SAVE_PATH = strcat('corrections_1e-6.csv');


skipped_trajectories = 0;
aux = [];
N = 30;

str = ['Generating dataset ...', num2str(0), '/', num2str(N)];
h = waitbar(0, str);
for i = 1:N
    BC = bc2correct(i, :);
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_3D(BC(2:end), tol);
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = Mat_ds(1,:);
    catch
        skipped_trajectories = skipped_trajectories + 1;
        return
    end

    aux = [aux; Mat_ds];
     
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
end
close(h)

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)
