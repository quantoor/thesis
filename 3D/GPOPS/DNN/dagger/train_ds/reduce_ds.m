clear
clc

ds_3000 = csvread('ds_train_3000.csv');
ds_reduced = ds_3000(1:50000,:);

csvwrite('ds_train_0.csv', ds_reduced)

figure()
hold on
grid on
plot3(ds_reduced(:,1),ds_reduced(:,2),ds_reduced(:,3),'b.')
axis equal