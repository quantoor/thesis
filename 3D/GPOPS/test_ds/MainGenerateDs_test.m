clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 100;
tol = 1e-5;
n_points = 100;

FILE_NAME = 'ds_test_5';
SAVE_PATH = strcat(FILE_NAME, '.csv');
TEMP_PATH = 'aux_temp.csv';

aux = [];
starting_idx = 1;

try
    aux = csvread(TEMP_PATH);
    starting_idx = size(aux,1)/n_points + 1;
catch
    %
end
str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(0, str);
for i = starting_idx:N
    % Random BC
    x0_rand = unifrnd(1550, 1950);
    y0_rand = unifrnd(-90, 90);
    z0_rand = unifrnd(1050, 1450);
    vx0_rand = unifrnd(-58, -52);
    if y0_rand > 0
        vy0_rand = unifrnd(-8, 0);
    else
        vy0_rand = unifrnd(0, 8);
    end
    vz0_rand = unifrnd(-28, -22);
    m0_rand = unifrnd(1250, 1350);
    BC = [x0_rand, y0_rand, z0_rand, vx0_rand, vy0_rand, vz0_rand, m0_rand];
    BC = [85.464932, 12.32613, 116.61943, -12.301833, -1.5891881, -15.917393, 1200.9255];

    % Compute trajectory
    tic
    [X, y, time] = GPOPS_3D(BC, tol);
    toc
    
    Mat_ds = [X, y, time];

    sample_vec = round(linspace(1, length(time), n_points));
    Mat_ds = Mat_ds(sample_vec, :);
    
    if i>1
        aux = csvread(TEMP_PATH);
    end
    aux = [aux; Mat_ds];

%     dlmwrite(TEMP_PATH, aux, 'delimiter', ',', 'precision', 8)

    figure()
    subplot(411)
    plot(time, X(:,1:3),'.')
    ylabel('Position [m]')    
    subplot(412)
    plot(time, X(:,4:6),'.')
    ylabel('Velocity [m/s]')    
    subplot(413)
    plot(time, X(:,7),'.')
    ylabel('Mass [kg]')    
    subplot(414)
    hold on
    plot(time, y(:,1:3),'.')
    plot(time, y(:,4),'--')
    ylabel('Acceleration [m/s^2]')
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
    return
end
close(h)

n_points = size(aux,1);

%%
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(FILE_NAME,'.txt'), 'w');
fprintf(logFileID, FILE_NAME);
fprintf(logFileID, '\nNumber of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);
