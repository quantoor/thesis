import subprocess

print("hyper call GPOPS called")
ROOT = 'python Main_DNN_dagger.py '

cases = []
cases.append('--layers 6 --n_hidden 32 --hyper_mode True')
cases.append('--layers 6 --n_hidden 64 --hyper_mode True')
cases.append('--layers 6 --n_hidden 128 --hyper_mode True')

cases.append('--layers 7 --n_hidden 32 --hyper_mode True')
cases.append('--layers 7 --n_hidden 64 --hyper_mode True')
cases.append('--layers 7 --n_hidden 128 --hyper_mode True')

cases.append('--layers 8 --n_hidden 32 --hyper_mode True')
cases.append('--layers 8 --n_hidden 64 --hyper_mode True')
cases.append('--layers 8 --n_hidden 128 --hyper_mode True')


for i in cases:
    subprocess.call(ROOT + i)

# tensorboard --logdir=path/to/log-directory
