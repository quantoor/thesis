import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import pickle
from scipy.integrate import ode
import scipy.optimize
from numpy.random import seed
from tensorflow import set_random_seed

import sys
sys.path.insert(0, '../../common3D')

import util, util_plot, Eom, ZEM_ZEV_3D
from elm import GenELMRegressor
from random_layer import MLPRandomLayer, RBFRandomLayer
import argparse
from decimal import Decimal


def TrainNetwork(aux, FLAGS, overwrite=False):
    seed(42)
    set_random_seed(42)
    try:
        model = pickle.load(open(aux.MODEL, 'rb'))
        if not overwrite:
            print('Model already exists, exit training.\n')
            return
    except:
        pass

    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    L = train_ds.shape[0]
    split = int(0.85*L)

    X_train = train_ds.iloc[:split, :7].values    # state
    y_train = train_ds.iloc[:split, 7:11].values  # target

    X_val = train_ds.iloc[split:, :7].values      # state
    y_val = train_ds.iloc[split:, 7:11].values    # target

    # Input scaling
    scaler = MinMaxScaler(feature_range=(0, 1))
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.transform(X_val)
    pickle.dump(scaler, open(aux.SCALER,'wb'))

    # =============================================================================
    # ELM
    # =============================================================================
    neurons_list = [100,300,500,800,900, 1000,1200,1400, 1500, 1600,1700,1800]
    # neurons_list = [1800,2000,2200,2500,3000]
    # neurons_list = [3000, 3200, 3500, 3800, 4000]
    old_rmse = 1
    rmse_train_vec = []
    rmse_val_vec = []
    for n_neurons in neurons_list:

        srhl_rbf = RBFRandomLayer(n_hidden=n_neurons, rbf_width=0.1, random_state=42)
        model = GenELMRegressor(srhl_rbf)

        # Train
        print("Training with " + str(n_neurons) + " neurons...")
        model.fit(X_train, y_train)

        # Train RMSE
        y_pred_train = model.predict(X_train)

        mse_train = np.mean((y_pred_train - y_train)**2)
        rmse_train = np.sqrt(mse_train)
        rmse_train_vec.append(rmse_train)

        # Validation RMSE
        y_pred_val = model.predict(X_val)

        mse_val = np.mean((y_pred_val - y_val)**2)
        rmse_val = np.sqrt(mse_val)
        rmse_val_vec.append(rmse_val)

        if rmse_val < old_rmse:
            best_model = model
            best_neurons = n_neurons
        old_rmse = rmse_val
        print('Train RMSE: %.2E' % Decimal(rmse_train))
        print('Validation RMSE: %.2E' % Decimal(rmse_val))
        print()

    util_plot.PlotTrainValLossELM(aux, rmse_train_vec, rmse_val_vec, neurons_list)

    print('Best number of neurons: ' + str(best_neurons))

    # Save best model
    best_model.hidden_activations_ = []
    best_model.hidden_layer.input_activations_ = []
    pickle.dump(best_model, open(aux.MODEL, 'wb'))
    print("Best model saved.")


def Main(FLAGS):
    # =============================================================================
    #  Main parameters
    # =============================================================================
    type = 'dagger'
    net = 'ELM'
    if FLAGS.iter>-1:
        iter = str(FLAGS.iter)
    else:
        iter = '0'
    dist = True
    start_point = 0.7
    end_point = 0.93
    d_bc = 5
    aux = util.Aux(type, net, iter, d_bc, dist, start_point=start_point, end_point=end_point)

    # =============================================================================
    #  Main
    # =============================================================================
    # TrainNetwork(aux, FLAGS, overwrite=True)
    # rmse, acc = util.GPOPS_EvaluateNetwork(aux)

    util.GPOPS_MonteCarloSimulation(aux, overwrite=True)
    # util.GPOPS_GetResults(aux, rmse, acc)

    # util.GPOPS_CollectStatesToCorrect(aux)
    # util.ZEM_ZEV_CollectBCForDAgger(aux, plot=0)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--iter', type=int, default=-1, help='iteration')
    # parser.add_argument('--n_hidden', type=int, default=128, help='hidden neurons')
    # parser.add_argument('--hyper_mode', type=bool, default=False, help='If true, just compare different models')
    # parser.add_argument('--layers', type=int, default=4, help='hidden layers')
    # parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
    # parser.add_argument('--batch_size', type=int, default=8, help='mini batch size')
    # parser.add_argument('--epochs', type=int, default=500, help='epochs')
    # parser.add_argument('--l2', type=float, default=1e-6, help='L2 penalty regularization')
    # parser.add_argument('--custom_name', type=str, default=None, help='custom name for tensorboard folder')

    FLAGS = parser.parse_args()
    Main(FLAGS)
