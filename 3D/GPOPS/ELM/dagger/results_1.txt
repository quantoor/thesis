Dagger iteration: 1

Train set points:	3.21E+04
Test RMSE:		5.19E-02
Test accuracy:		1.00

Monte Carlo simulation:
Success rate:		5.00 %
Average final position:	[6.22, 2.28, 9.20]
Average final velocity:	[0.48, 0.42, 0.10]

Distance CM:		8.62E-04