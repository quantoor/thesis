Dagger iteration: 0

Train set points:	3.00E+04
Test RMSE:		-1.00E+00
Test accuracy:		-1.00

Monte Carlo simulation:
Success rate:		64.00 %
Average final position:	[7.18, 7.37, 1.28]
Average final velocity:	[2.01, 1.98, 3.32]

Distance CM:		6.62E-03