clear; close all; clc
rng('shuffle')
addpath('../../../../common3D/matlab')

dagger_it = 3;
tol = 1e-2;

BC2CORRECT = strcat('bc2correct_ord_4.csv');
CORRECTIONS = strcat('corrections_4.csv');
OLD_TRAIN_DS = strcat('../train_ds/ds_train_', num2str(dagger_it), '.csv'); 
TEMP = 'aux_temp_4.csv';

BC_mat = csvread(BC2CORRECT);
old_train_ds = csvread(OLD_TRAIN_DS);


%% plot states to correct
figure()
hold on
plot(old_train_ds(1:10:end,1), old_train_ds(1:10:end,3),'b.')
plot(BC_mat(:,1), BC_mat(:,3),'r.')
grid on
axis equal
xlabel('Position [m]')
ylabel('Velocity [m/s]')
close all


%% correct labels
N = size(BC_mat, 1);
aux = [];
starting_idx = 1;
skipped_trajectories = 0;

try
    aux = csvread(TEMP);
    starting_idx = size(aux,1)+1;
catch
    %
end

str = ['Generating dataset_4 ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(starting_idx/N, str);
for i = starting_idx:N
    str = ['Generating dataset_4 ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
    
    BC = BC_mat(i, :);
    disp(i)
    disp(BC(3))
    disp(BC(6))
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_3D(BC, tol);    
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = Mat_ds(1, :);
    catch
        skipped_trajectories = skipped_trajectories + 1;
        continue
    end
    
    aux = [aux; Mat_ds];
    
    dlmwrite(TEMP, aux, 'delimiter', ',', 'precision', 8)
end
close(h)
rmpath('../../../../common3D/matlab')