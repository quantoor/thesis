function dxdt = Eom_ZEM_ZEV(t, X, y_pred)
x = X(1);
y = X(2);
z = X(3);
vx = X(4);
vy = X(5);
vz = X(6);

g0 = 1.622;

xdot = vx;
ydot = vy;
zdot = vz;
vxdot = y_pred(1);
vydot = y_pred(2);
vzdot = -g0 + y_pred(3);

dxdt = [xdot, ydot, zdot, vxdot, vydot, vzdot]';
end