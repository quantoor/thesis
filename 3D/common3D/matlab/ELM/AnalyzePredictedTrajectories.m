function [SR, finalState] = AnalyzePredictedTrajectories(aux)

% load datasets
predictedTrajectories = load(aux.PRED_TRAJ);
predictedTrajectories = predictedTrajectories.predictedTrajectories;

optimalTrajectories = load(aux.TEST_DS);

% initialize variables
n_traj = size(optimalTrajectories, 1)/aux.n_steps;

SR = 0;
finalState_vec = zeros(n_traj, 2);

for i = 1:n_traj    
    predTraj = predictedTrajectories{i};
    finalState_vec(i, :) = abs(predTraj(end, 1:2));
    
    if predTraj(end, 1) < aux.SR_criteria(1) && predTraj(end, 2) < aux.SR_criteria(2)
        SR = SR + 1;
    end
    
end

SR = SR/n_traj*100;
finalState = sum(finalState_vec, 1)/n_traj;
end