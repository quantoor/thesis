function [Xfix, yfix, tfix] = ZEM_ZEV_SampleFix(X, y, tvect, n_steps)

tfix = linspace(0, tvect(end), n_steps); % time vector with fixed step
Xfix = zeros(n_steps, size(X,2));                % # of steps fixed to 150
yfix = zeros(n_steps, size(y,2));

[tvect, index] = unique(tvect); % remove doubles

% interpolation
for i = 1:size(X,2) % for each column of X
    col = X(:, i);
    Xfix(:, i) = interp1(tvect, col(index), tfix); % linear interpolation
end

for i = 1:size(y,2) % for each column of y
    col = y(:, i);
    yfix(:, i) = interp1(tvect, col(index), tfix); % linear interpolation
end

% output column vectors
tfix = tfix';
end