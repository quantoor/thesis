function GPOPS_Dagger(TRAIN_PATH, SAVE_PATH, BC_PATH, TEXT_PATH, tol, LSTM)

BC_mat = csvread(BC_PATH);
old_train_ds = csvread(TRAIN_PATH);

N = size(BC_mat, 1);

aux = [];
skipped_trajectories = 0;

% str = ['Generating dataset ... 0 / ', num2str(N)];
% h = waitbar(0, str);

parfor i = 1:N
    BC = BC_mat(i,:);
        
    % Compute trajectory    
    tic
    output = GPOPS_ComputeTrajectory(BC, tol);
    toc
        
%     plot_gpops_solution(output)

    % Extract solution
    solution = output.result.solution;
    time = solution.phase(1).time;
    state = solution.phase(1).state;
    control = solution.phase(1).control;

    % Preprocess data
    state = [state(:,3), state(:,6), state(:,7)];
    y = control(:,4);
    
    Mat_ds = [state(1,:), y(1)>2200, time(1)];
    aux = [aux; Mat_ds];
    

    %     figure(1)
    %     subplot(211)
    %     grid on
    %     plot(time, state(:,1), 'bo', tfix, Xfix(:,1), 'r.')    
    %     subplot(212)
    %     grid on
    %     plot(time, y, 'bo', tfix, 1000+2400*yfix, 'r.')    
    %     close(h)
    %     return
%     str = ['Generating dataset ... ', num2str(i),' / ', num2str(N)];
%     waitbar(i/N, h, str)
end
ds_augmented = [old_train_ds; aux];

% Shuffle if not LSTM
if ~LSTM
    ds_augmented_shuffled = randblock(ds_augmented, [1, size(ds_augmented,2)]);
end

% close(h)

% save backup
dlmwrite('temp.csv', ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)
fprintf('\nOld training set: %d states\n', size(old_train_ds,1))
fprintf('New states: %d states\n', size(aux,1))
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))
fprintf('Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1))

logFileID = fopen(TEXT_PATH, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(old_train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Skipped trajectories: %d\n', skipped_trajectories);
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fprintf(logFileID, 'Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1));
fclose(logFileID);

dlmwrite(SAVE_PATH, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)
end