function tgo = ComputeTgo(r, v, tgo)

g = [0; 0; -1.622];
rf = zeros(3,1);
vf = zeros(3,1);

%% compute Tgo
A = g'*g;
B = -2*(v'*v + vf'*v + vf'*vf);
C = 12*(rf-r)'*(v+vf);
D = -18*(rf-r)'*(rf-r);

fun = @(T) A*T.^4+B*T.^2+C*T+D; % function for Tgo calculation

% set initial guess for fzero
if tgo<3 && tgo>1
    XX0 = [0,3];
elseif tgo<1
    XX0 = [0,1];
else
    XX0 = [0,80];
end

tgo = fzero(fun, XX0);

end