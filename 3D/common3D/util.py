import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from decimal import Decimal
import matplotlib.patches as mpatches
import six
import pickle
from keras.models import load_model
import keras
from scipy.integrate import ode

import Eom, util_plot, ZEM_ZEV_3D


##########################################################################################
# AUX
##########################################################################################
class Aux():
    def __init__(self, type, net, iter, d_bc, disturbance=None, start_point=None, end_point=None):
        self.net = net
        self.iter = iter
        self.disturbance = disturbance
        self.start_point = start_point
        self.end_point = end_point
        self.n_bins = 20
        self.n_steps = 100
        self.dt = 0.1
        self.SR_criteria = [7.5, 0.5]
        self.d_bc = d_bc

        self.TRAIN_DS = type + '/train_ds/ds_train_' + iter + '.csv'
        self.BC4DAGGER = type + '/train_ds/bc4dagger_' + iter + '.csv'
        self.LOGS = type + '/train_ds/tsb_' + iter

        self.TEST_DS = '../test_ds/ds_test_' + iter + '.csv'

        self.PRED_TRAJ = type + '/predictions/predictedTrajectories_' + iter + '.npy'
        self.BC2CORRECT = type + '/predictions/bc2correct_' + iter + '.csv'
        self.CORRECTIONS = type + '/predictions/corrections_' + iter + '.csv'

        if net == 'DNN':
            self.MODEL = type + '/model/DNN_' + iter + '.h5'
        elif net == 'ELM':
            self.MODEL = type + '/model/ELM_' + iter + '.p'
        else:
            raise Exception('Net type not valid.')

        self.SCALER = type + '/model/scaler_' + net + '_' + iter + '.p'
        self.TRAIN_LOG = type + '/model/training_DNN_' + iter + '.csv'
        self.TRAIN_HIST = type + '/model/train_hist_DNN_' + iter + '.p'

        self.TEXT_FILE = type + '/results_' + iter + '.txt'
        self.FIG_ROOT =  type + '/img/' + iter + '_'


##########################################################################################
# Monte Carlo Simulation
##########################################################################################
def ZEM_ZEV_MonteCarloSimulation(aux, overwrite=False):
    try:
        np.load(aux.PRED_TRAJ, allow_pickle=True)
        if not overwrite:
            print('Predicted trajectories found, exit Monte Carlo.')
            return
    except:
        pass
    # =============================================================================
    # Data and Model load
    # =============================================================================
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        raise Exception('Network not recognized')
    scaler = pickle.load(open(aux.SCALER, 'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :8].values.astype('float32')  # pos, vel, tgo, mass
    tf_test = test_ds.iloc[:, 11].values.astype('float32') # final time

    n_traj = int(X_test.shape[0]/aux.n_steps)
    predictedTrajectories = []

    for i in range(n_traj):
        j = i*aux.n_steps
        BC = X_test[j, :]               # initial conditions of each test trajectory
        tf = tf_test[j+aux.n_steps-1]   # final time of each test trajectory

        predictedTrajectory = ZEM_ZEV_PredictTrajectory(aux, model, scaler, BC, tf)
        predictedTrajectories.append(predictedTrajectory)
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_traj))

    predictedTrajectories = np.array(predictedTrajectories)
    np.save(aux.PRED_TRAJ, predictedTrajectories)


def ZEM_ZEV_PredictTrajectory(aux, model, scaler, BC, t_max):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    n_features = 7
    landed = False

    # =============================================================================
    # Initial conditons
    # =============================================================================
    # initial condition for Eom: pos0, vel0, m0
    state0 = np.delete(BC, 6)

    # First input: pos0, vel0, tgo0
    X_i = np.delete(BC, 7).reshape(1,-1)
    Xf = np.zeros((1,6))
    tgo = BC[6]

    # First prediction
    if aux.net == 'DNN':
        y_x_pred, y_y_pred, y_z_pred = model.predict(scaler.transform(X_i))
        y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred]
    elif aux.net == 'ELM':
        y_pred = model.predict(scaler.transform(X_i))

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_state = BC.reshape(1,-1)  # predicted state matrix
    pred_t = y_pred.reshape(1,-1)  # predicted thrust matrix

    time_int = np.arange(0, t_max+10, aux.dt)
    time_int = time_int.reshape(-1, 1)

    for i in range(len(time_int)-1):
        # add disturbances
        if aux.disturbance:
            y_pred *= np.random.uniform(0.8, 1.2, 3).reshape(1,-1)

        t0 = time_int[i, 0]
        tf = time_int[i+1, 0]

        x_int = ode(Eom.Eom_ZEM_ZEV)
        x_int.set_integrator('dopri5', rtol=1e-8) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(state0.reshape(-1,1), t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(y_pred.flatten())
            x_int.integrate(tf, step=True)

        X_new = x_int.y.flatten() # pos, vel, mass
        tgo = ZEM_ZEV_3D.ComputeTgo(X_new[:-1], Xf.flatten(), tgo)

        X_new = np.insert(X_new, 6, tgo.reshape(1,-1)) # pos, vel, tgo, mass

        # Make new prediction
        if aux.net == 'DNN':
            y_x_pred, y_y_pred, y_z_pred = model.predict(scaler.transform(X_new[:-1].reshape(1,-1)))
            y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred]
        elif aux.net == 'ELM':
            y_pred = model.predict(scaler.transform(X_new[:-1].reshape(1,-1)))

        pred_state = np.r_[pred_state, X_new.reshape(1,-1)]
        pred_t = np.r_[pred_t, y_pred.reshape(1,-1)]

        i = i+1
        if x_int.y[2] < 0:
            landed = True
            break

        if x_int.y[5] > 0:
            break

        state0 = x_int.y # define new initial conditions

    if not landed:
        finalPos = min(pred_state[:,2])
        print("Not landed, lowest altitude reached: " + str(finalPos))

    print(pred_state[-1,:6])
    return np.c_[pred_state, pred_t, time_int[:i+1]]


def GPOPS_MonteCarloSimulation(aux, overwrite=False):
    try:
        np.load(aux.PRED_TRAJ, allow_pickle=True)
        if not overwrite:
            print('Predicted trajectories found, exit Monte Carlo.')
            return
    except:
        pass
    # =============================================================================
    # Data and Model load
    # =============================================================================
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    scaler = pickle.load(open(aux.SCALER, "rb"))

    opt_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_opt = opt_ds.iloc[:, :7].values.astype('float32')      # state
    time_opt = opt_ds.iloc[:, -1].values.astype('float32')    # time

    n_trajectories = int(X_opt.shape[0]/aux.n_steps);
    predictedTrajectories = []

    for i in range(n_trajectories):
        j = i*aux.n_steps
        BC = X_opt[j, :]
        time = time_opt[j:j+aux.n_steps]

        predictedTrajectory = GPOPS_PredictTrajectory(aux, model, scaler, BC, time[-1])
        predictedTrajectories.append(predictedTrajectory)
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_trajectories))

    predictedTrajectories = np.array(predictedTrajectories)
    np.save(aux.PRED_TRAJ, predictedTrajectories)


def GPOPS_PredictTrajectory(aux, model, scaler, BC, t_max):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    n_features = 7
    landed = False

    # First input
    X_i = BC[:n_features].reshape(1, n_features)

    # First prediction
    if aux.net == 'DNN':
        y_x_pred, y_y_pred, y_z_pred, y_c_pred = model.predict(scaler.transform(X_i))
        y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred, y_c_pred[0].argmax(axis=-1)]
    elif aux.net == 'ELM':
        y_pred = model.predict(scaler.transform(X_i))

    # =============================================================================
    # Initial conditons
    # =============================================================================
    state0 = np.array(BC) # vector of initial conditions

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_state = state0.reshape(1, -1)        # predicted state
    pred_t = y_pred.reshape(1, -1)            # predicted thrust

    time_int = np.arange(0, t_max+10, aux.dt)
    time_int = time_int.reshape(-1, 1)

    for i in range(len(time_int)-1):
        t0 = time_int[i, 0]
        tf = time_int[i+1, 0]

        x_int = ode(Eom.Eom_GPOPS)
        x_int.set_integrator('dopri5', rtol=1e-5) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(state0, t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(y_pred.flatten())
            x_int.integrate(tf, step=True)

        x_new = x_int.y.reshape(1, -1)

        # Make new prediction
        if aux.net == 'DNN':
            y_x_pred, y_y_pred, y_z_pred, y_c_pred = model.predict(scaler.transform(x_new))
            y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred, y_c_pred[0].argmax(axis=-1)]
        elif aux.net == 'ELM':
            y_pred = model.predict(scaler.transform(x_new))

        pred_state = np.r_[pred_state, x_new]
        pred_t = np.r_[pred_t, y_pred.reshape(1,-1)]

        i = i+1
        if x_int.y[2] <= 0:
            landed = True
            print('Altitude zero reached')
            break

        if x_int.y[5] > 0:
            break

        state0 = x_int.y # define new initial conditions


    if not landed:
        finalPos = min(pred_state[:,2])
        print("Not landed, lowest altitude reached: " + str(finalPos))

    print(pred_state[-1,:6])
    return np.c_[pred_state, pred_t, time_int[:i+1]]


##########################################################################################
# EVALUATE NETWORK
##########################################################################################
def ZEM_ZEV_EvaluateNetwork(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        raise Exception('Network not recognized')

    scaler = pickle.load(open(aux.SCALER,'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :7].values
    y_test = test_ds.iloc[:, 8:11].values

    X_test = scaler.transform(X_test)

    if aux.net == 'DNN':
        y_x_pred, y_y_pred, y_z_pred = model.predict(X_test)
        y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred]
    elif aux.net == 'ELM':
        y_pred = model.predict(X_test)

    # compute RMSE
    mse = np.mean((y_pred - y_test) ** 2, axis=0)
    rmse = np.sqrt(mse)

    util_plot.PlotRegressionCurve(aux, y_pred, y_test)

    print('\nTest RMSE: [%.2f, %.2f, %.2f]' % (rmse[0], rmse[1], rmse[2]))
    return np.sum(rmse)


def GPOPS_EvaluateNetwork(aux):
    model = load_model(aux.MODEL)
    scaler = pickle.load(open(aux.SCALER,'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :7].values
    y_test = test_ds.iloc[:, 7:11].values

    y_x_pred, y_y_pred, y_z_pred, y_c_pred = model.predict(scaler.transform(X_test))
    y_pred = np.c_[y_x_pred, y_y_pred, y_z_pred, y_c_pred.argmax(axis=-1)]

    # compute RMSE
    mse = np.mean((y_pred[:,:3] - y_test[:,:3])**2, axis=0)
    rmse = np.sqrt(mse)

    # compute Accuracy
    err = abs(y_pred[:,3] - y_test[:,3])
    acc = np.mean(1-err)

    util_plot.PlotRegressionCurve(aux, y_pred[:,:3], y_test[:,:3])
    util_plot.PlotConfusionMatrix(aux, y_pred[:,3], y_test[:,3])

    print('\nTest RMSE: [%.2f, %.2f, %.2f]' % (rmse[0], rmse[1], rmse[2]))
    print('Test accuracy: %.2f' % acc)
    return np.sum(rmse), acc


##########################################################################################
# COLLECT STATES TO CORRECT
##########################################################################################
def ZEM_ZEV_CollectStatesToCorrect(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    bc2correct = []
    k = 0

    for i in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[i]
        L = predTraj.shape[0]
        bc2correct_vec = np.arange(int(L*aux.start_point), L, aux.d_bc)

        for j in bc2correct_vec:
            aux_vec = np.c_[np.array([k]).reshape(1,-1), predTraj[j, :6].reshape(1,-1)]
            bc2correct.append(aux_vec)
            k += 1

    bc2correct = np.array(bc2correct).reshape(-1,7)
    np.savetxt(aux.BC2CORRECT, bc2correct, delimiter=',')
    print('States to correct: ' + str(len(bc2correct)))


def GPOPS_CollectStatesToCorrect(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    bc2correct = []

    for i in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[i]
        L = len(predTraj)

        bc2correct_vec = np.arange(int(L*aux.start_point), int(L*aux.end_point), aux.d_bc)

        for j in bc2correct_vec:
            bc2correct.append(predTraj[j, :7])

    bc2correct = np.array(bc2correct)
    np.savetxt(aux.BC2CORRECT, bc2correct, delimiter=',')
    print('States to correct: ' + str(len(bc2correct)))


##########################################################################################
# COLLECT BC FOR DAGGER
##########################################################################################
def ZEM_ZEV_CollectBCForDAgger(aux, plot=0):
    # load train dataset
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    x = train_ds.iloc[:, :6].values

    # load states corrected
    corrections = pd.read_csv(aux.CORRECTIONS, header=None)
    y_correct = corrections.iloc[:, 8:11].values

    # load predicted trajectory
    predictedTrajectory = np.load(aux.PRED_TRAJ, allow_pickle=True)

    bc4dagger = []
    finalState_vec = []
    mse_vec = []
    j = 0 # counter to plot the corrections

    # collect states for dagger
    for idx in range(len(predictedTrajectory)):
        predTraj = predictedTrajectory[idx]
        if plot:
            fig, (ax1, ax2, ax3) = plt.subplots(3,1)
            print(predTraj[-1,:6])
        L = predTraj.shape[0]
        bc_vec = np.arange(int(L*aux.start_point), L-1, aux.d_bc)

        ############################################################################

        for i in bc_vec:
            time = predTraj[i, 11]
            y_pred = predTraj[i, 8:11]
            y_opt = y_correct[j, :]

            try:
                if plot:
                    ax1.plot(time, y_pred[0], 'r.')
                    ax1.plot(time, y_opt[0], 'b.')

                    ax2.plot(time, y_pred[1], 'r.')
                    ax2.plot(time, y_opt[1], 'b.')

                    ax3.plot(time, y_pred[2], 'r.')
                    ax3.plot(time, y_opt[2], 'b.')

                err = abs(y_pred - y_opt)
                mse_vec.append(err**2)
                if np.any(err>0.05) and np.all(err<1):
                    if plot:
                        ax1.plot(time, y_pred[0], 'ko', markersize=7, alpha=0.3)
                        ax2.plot(time, y_pred[1], 'ko', markersize=7, alpha=0.3)
                        ax3.plot(time, y_pred[2], 'ko', markersize=7, alpha=0.3)
                    bc4dagger.append(predTraj[i,:])
            except:
                print('Point ' + str(i) + ' not available\n')
            j += 1


        if plot:
            red = mpatches.Patch(color='red', label='Predicted')
            blue = mpatches.Patch(color='blue', label='Optimal')
            black = mpatches.Patch(color='black', alpha=0.3, label='States collected')
            plt.legend(handles=[blue, red, black])
            ax3.set_xlabel('Time [s]')
            ax1.set_ylabel('Acc x [m/s^2]')
            ax2.set_ylabel('Acc y [m/s^2]')
            ax3.set_ylabel('Acc z [m/s^2]')
            plt.show()

    print('New states collected for Dagger: ' + str(len(bc4dagger)))
    np.savetxt(aux.BC4DAGGER, bc4dagger, delimiter=',')

    mse_vec = np.array(mse_vec)
    mse = mse_vec.mean()
    return mse


def GPOPS_CollectBCForDAgger(aux, plot=0):
    # load train dataset
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    x = train_ds.iloc[:, :6].values

    # load states corrected
    corrections = pd.read_csv(aux.BC4DAGGER, header=None)
    y_correct = corrections.iloc[:, 7:10].values

    # load predicted trajectory
    predictedTrajectory = np.load(aux.PRED_TRAJ, allow_pickle=True)

    bc4dagger = []
    finalState_vec = []
    mse_vec = []
    j = 0 # counter to plot the corrections

    # collect states for dagger
    for idx in range(len(predictedTrajectory)):
        predTraj = predictedTrajectory[idx]
        if plot:
            fig, (ax1, ax2, ax3) = plt.subplots(3,1)
            print(predTraj[-1,:6])
        L = predTraj.shape[0]
        bc_vec = np.arange(int(L*aux.start_point), int(L*aux.end_point), aux.d_bc)

        ############################################################################

        for i in bc_vec:
            time = predTraj[i, 11]
            y_pred = predTraj[i, 7:10]
            y_opt = y_correct[j, :]

            try:
                if plot:
                    ax1.plot(time, y_pred[0], 'r.')
                    ax1.plot(time, y_opt[0], 'b.')

                    ax2.plot(time, y_pred[1], 'r.')
                    ax2.plot(time, y_opt[1], 'b.')

                    ax3.plot(time, y_pred[2], 'r.')
                    ax3.plot(time, y_opt[2], 'b.')

                err = abs(y_pred - y_opt)
                mse_vec.append(err**2)
                if (np.any(err>0.03) and np.all(err<1)) or err[-1]:
                    if plot:
                        ax1.plot(time, y_pred[0], 'ko', markersize=7, alpha=0.3)
                        ax2.plot(time, y_pred[1], 'ko', markersize=7, alpha=0.3)
                        ax3.plot(time, y_pred[2], 'ko', markersize=7, alpha=0.3)
                    bc4dagger.append(predTraj[i,:])
            except:
                print('Point ' + str(i) + ' not available\n')
            j += 1

        red = mpatches.Patch(color='red', label='Predicted')
        blue = mpatches.Patch(color='blue', label='Optimal')
        black = mpatches.Patch(color='black', alpha=0.3, label='States collected')

        if plot:
            plt.legend(handles=[blue, red, black])
            ax3.set_xlabel('Time [s]')
            ax1.set_ylabel('Acc x [m/s^2]')
            ax2.set_ylabel('Acc y [m/s^2]')
            ax3.set_ylabel('Acc z [m/s^2]')
            plt.show()
        else:
            plt.close()

    print('New states collected for Dagger: ' + str(len(bc4dagger)))
    np.savetxt(aux.BC4DAGGER, bc4dagger, delimiter=',')

    mse_vec = np.array(mse_vec)
    mse = mse_vec.mean()
    return mse


##########################################################################################
# GET AND SAVE RESULTS
##########################################################################################
def ZEM_ZEV_GetResults(aux, rmse):
    SR, finalState = AnalyzePredictedTrajectories(aux)

    util_plot.PlotHistogram1d(aux)
    D_CM = util_plot.PlotHistograms(aux)

    rmse = np.array(rmse).reshape(1,-1)
    SR = np.array(SR).reshape(1,-1)
    finalState = np.array(finalState).reshape(1,-1)
    D_CM = np.array(D_CM).reshape(1,-1)

    results_numpy = np.c_[rmse, SR, finalState, D_CM].flatten()
    np.save(aux.TEXT_FILE[:-4], results_numpy)
    ZEM_ZEV_SaveResults(aux, results_numpy)


def GPOPS_GetResults(aux, rmse, acc):
    SR, finalState = AnalyzePredictedTrajectories(aux)

    util_plot.PlotHistogram1d(aux)
    D_CM = util_plot.PlotHistograms(aux)

    rmse = np.array(rmse).reshape(1,-1)
    acc = np.array(acc).reshape(1,-1)
    SR = np.array(SR).reshape(1,-1)
    finalState = np.array(finalState).reshape(1,-1)
    D_CM = np.array(D_CM).reshape(1,-1)

    results_numpy = np.c_[rmse, acc, SR, finalState, D_CM].flatten()
    np.save(aux.TEXT_FILE[:-4], results_numpy)
    GPOPS_SaveResults(aux, results_numpy)


def ZEM_ZEV_SaveResults(aux, results):
    f = open(aux.TEXT_FILE, 'w')
    f.write('Dagger iteration: ' + aux.iter)
    f.write('\n')
    f.write('\nTrain set points:\t%.2E' % (Decimal(GetDatasetDimension(aux.TRAIN_DS))))
    f.write('\nTest RMSE:\t\t%.2E' % Decimal(results[0]))
    f.write('\n')
    f.write('\nMonte Carlo simulation:')
    f.write('\nSuccess rate:\t\t%.2f %%' % results[1])
    f.write('\nAverage final position:\t[%.2f, %.2f, %.2f]' % (results[2], results[3], results[4]))
    f.write('\nAverage final velocity:\t[%.2f, %.2f, %.2f]' % (results[5], results[6], results[7]))
    f.write('\n')
    f.write('\nDistance CM:\t\t%.2E' % (Decimal(results[8])))
    f.close()


def GPOPS_SaveResults(aux, results):
    f = open(aux.TEXT_FILE, 'w')
    f.write('Dagger iteration: ' + aux.iter)
    f.write('\n')
    f.write('\nTrain set points:\t%.2E' % (Decimal(GetDatasetDimension(aux.TRAIN_DS))))
    f.write('\nTest RMSE:\t\t%.2E' % Decimal(results[0]))
    f.write('\nTest accuracy:\t\t%.2f' % results[1])
    f.write('\n')
    f.write('\nMonte Carlo simulation:')
    f.write('\nSuccess rate:\t\t%.2f %%' % results[2])
    f.write('\nAverage final position:\t[%.2f, %.2f, %.2f]' % (results[3], results[4], results[5]))
    f.write('\nAverage final velocity:\t[%.2f, %.2f, %.2f]' % (results[6], results[7], results[8]))
    f.write('\n')
    f.write('\nDistance CM:\t\t%.2E' % (Decimal(results[9])))
    f.close()


##########################################################################################
# OTHER UTILS
##########################################################################################
def AnalyzePredictedTrajectories(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    testTrajectories = Convert_csv2npy(aux.TEST_DS, aux.n_steps)

    SR1, SR2 = aux.SR_criteria
    N = len(predictedTrajectories)
    finalPos_vec = []
    finalVel_vec = []
    # finalState_vec = []
    SR = 0
    # D_CM_vec = []
    massOptimality_vec = []

    # Accuracy on y_t and y_theta
    for i in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[i]
        testTraj = testTrajectories[i]

        # Final pos
        finalPos = predTraj[-1, :3]
        if np.all(finalPos<10):
            print(finalPos)
            finalPos_vec.append(finalPos)

        # Final vel
        finalVel = predTraj[-1, 3:6]
        if not finalPos[2] < 0.1:
            finalVel[2] = np.nan
        if np.all(finalPos<10):
            finalVel_vec.append(finalVel)

        # Final state
        # finalState = predTraj[-1, :6]
        # finalState_vec.append(finalState)

        # Von mises distance
        # D_CM = ComputeD_CM(aux, predTraj, testTraj)
        # D_CM_vec.append(D_CM)

        # Mass optimality
        m0 = testTraj[0, 7]
        mf_pred = predTraj[-1, 7]
        mf_test = testTraj[-1, 7]
        m_consumed_pred = m0 - mf_pred
        m_condumed_test = m0 - mf_test
        massOptimality = m_consumed_pred / m_condumed_test
        massOptimality_vec.append(massOptimality)

        for j in range(len(predTraj)):
            x, y, z, vx, vy, vz = predTraj[j,:6]
            if abs(z) < SR1 and abs(vz) < SR2:
                SR += 1
                break

    finalPos = np.sum(abs(np.asarray(finalPos_vec)), axis=0) / len(finalPos_vec)
    finalVel = np.nansum(abs(np.asarray(finalVel_vec)), axis=0) / len(finalVel_vec)

    finalState = np.c_[finalPos.reshape(1,-1), finalVel.reshape(1,-1)].flatten()
    # finalState_vec = np.array(finalState_vec)
    # finalState = np.sum(abs(finalState_vec), axis=0) / len(finalState_vec)
    print("\nAverage distance of final state from target:")
    print(finalState)

    SR = SR*100/N
    print('\nSuccess rate:')
    print(SR)

    # D_CM = np.mean(D_CM_vec)
    # print('\nVon Mises Distance:')
    # print(D_CM)

    massOptimality = np.mean(massOptimality_vec)
    print('\nMass optimality:')
    print(massOptimality)
    print()
    return SR, finalState


def ComputeD_CM(aux, predTraj, optTraj):
    X_opt = optTraj
    X_pred = predTraj
    n_bins = aux.n_bins

    n_opt, (bins_x_opt, bins_y_opt, bins_z_opt, bins_vx_opt, bins_vy_opt, bins_vz_opt) = np.histogramdd(X_opt, bins=n_bins, normed=True)
    n_pred, (bins_x_pred, bins_y_pred, bins_z_pred, bins_vx_pred, bins_vy_pred, bins_vz_pred) = np.histogramdd(X_pred, bins=n_bins, normed=True)

    # A = n_opt * np.diff(bins_x_opt) * np.diff(bins_y_opt) * np.diff(bins_z_opt) * np.diff(bins_vx_opt) * np.diff(bins_vy_opt) * np.diff(bins_vz_opt)
    # print( np.sum(A))
    # A = n_pred * np.diff(bins_x_pred) * np.diff(bins_y_pred) * np.diff(bins_z_pred) * np.diff(bins_vx_pred) * np.diff(bins_vy_pred) * np.diff(bins_vz_pred)
    # print( np.sum(A))

    ######################################################################################
    diff_distr = abs(n_opt - n_pred) * np.diff(bins_x_opt) * np.diff(bins_y_opt) * np.diff(bins_z_opt) * np.diff(bins_vx_opt) * np.diff(bins_vy_opt) * np.diff(bins_vz_opt)
    D_CM = np.sum(diff_distr**2)
    return D_CM


def Convert_csv2npy(CSV, n_steps):
    ds = pd.read_csv(CSV, header=None)
    n_traj = int(ds.shape[0]/n_steps)

    optimalTrajectories = np.zeros([n_traj, n_steps, 12])
    for i in range(n_traj):
        currentTrajectory = ds.iloc[i*n_steps:i*n_steps+n_steps,:].values.astype('float32')
        optimalTrajectories[i, :, :] = currentTrajectory

    return np.array(optimalTrajectories)


def Convert_npy2csv(DS):
    ds_npy = np.load(DS, allow_pickle=True)
    ds_aux = np.empty((0, ds_npy[0].shape[1]))
    N = len(ds_npy)

    for i in range(N):
        ds_aux = np.append(ds_aux, ds_npy[i], axis=0)
    return ds_aux


def GetDatasetDimension(DS):
    if DS[-3:] == 'csv':
        ds = pd.read_csv(DS, header=None)
    elif DS[-3:] == 'npy':
        ds = pd.DataFrame(Convert_npy2csv(DS))
    else:
        raise Exception('Dataset path not recognized')

    n_points = int(len(ds.iloc[:,0]))
    return n_points


def GetCallBacks(aux, FLAGS):
    call_backs = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-5, patience=15),
        keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=10, cooldown=1)]

    if FLAGS.hyper_mode:
        if FLAGS.custom_name != None:
            log_dir = './dagger/logs/train_ds_' + aux.iter + '/' + FLAGS.custom_name
        else:
            log_dir = './dagger/logs/train_ds_' + aux.iter + '/' + GetHyperString(FLAGS)
    else:
        if FLAGS.custom_name != None:
            log_dir = aux.LOGS + '/' + FLAGS.custom_name
        else:
            log_dir = aux.LOGS + '/' + GetHyperString(FLAGS)
        call_backs.append(keras.callbacks.ModelCheckpoint(monitor='val_loss', filepath=aux.MODEL, save_best_only=True))
        call_backs.append(keras.callbacks.CSVLogger(aux.TRAIN_LOG, separator=',', append=False))

    call_backs.append(keras.callbacks.TensorBoard(log_dir=log_dir, write_graph=False))
    return call_backs


def GetHyperString(FLAGS):
    layers = FLAGS.layers
    n_hidden = FLAGS.n_hidden
    lr = FLAGS.lr
    batch_size = FLAGS.batch_size
    l2 = FLAGS.l2

    layers = 'layers' + str(layers)
    n_hidden = '_hid' + str(n_hidden)
    lr_aux = str('%.0e' % Decimal(lr))
    lr = '_lr' + lr_aux[0] + 'e-' + lr_aux[-1]
    batch_size = '_batch' + str(batch_size)

    if l2 == 0:
        l2 = '_reg0'
    else:
        l2_aux = str('%.0e' % Decimal(l2))
        l2 = '_reg' + l2_aux[0] + 'e-' + l2_aux[-1]

    hyper_string = layers + n_hidden + lr + l2
    return hyper_string
