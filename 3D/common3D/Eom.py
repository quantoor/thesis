import numpy as np
import math
from scipy.optimize import fsolve


def Eom_GPOPS(t, X, y_pred):
    x, y, z, vx, vy, vz, m = X
    y_x, y_y, y_z, y_c = y_pred

    # Parameters
    g0  = 1.622
    # alphaM = 5*10**-4 <== fuuuck!! super hidden bug
    alphaM = 1/200/9.81 # 1 / Isp / g0 earth

    if y_c > 0.5:
        T = 3400
    else:
        T = 1000
    # T = 1000 + y_c * 2400

    # EoM
    xdot = vx
    ydot = vy
    zdot = vz

    vxdot = y_x * T/m
    vydot = y_y * T/m
    vzdot = y_z * T/m - g0

    mdot = -T*alphaM

    dxdt = [xdot, ydot, zdot, vxdot, vydot, vzdot, mdot]
    return dxdt


def Eom_ZEM_ZEV(t, X, acc):
    x, y, z, vx, vy, vz, m = X

    # Parameters
    g0  = 1.622
    alphaM = 1/200/9.81

    # EoM
    xdot = vx
    ydot = vy
    zdot = vz

    vxdot = acc[0]
    vydot = acc[1]
    vzdot = acc[2] - g0

    acc_tot = np.sqrt(acc[0]**2 + acc[1]**2 + acc[2]**2)
    T = m * acc_tot
    mdot = -T*alphaM

    dxdt = [xdot, ydot, zdot, vxdot, vydot, vzdot, mdot]
    return dxdt
