import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.mlab import griddata
from scipy.interpolate import interp1d
import matplotlib.patches as mpatches
import pickle
import six
from keras.models import load_model
import keras
from sklearn.metrics import confusion_matrix

import util


##########################################################################################
# REGRESSION CURVE AND CONFUSION MATRIX
##########################################################################################
def PlotRegressionCurve(aux, y_pred, y_opt):
    fig = plt.figure(figsize=(12,6))

    plt.subplot(131)
    plt.grid()
    xvec = np.linspace(min(y_opt[:,0]), max(y_opt[:,0]), 100)
    plt.plot(y_opt[:,0], y_pred[:,0], '.')
    plt.plot(xvec, xvec, '--')
    plt.title('x')
    plt.ylabel('Predictions')
    plt.xlabel('Targets')

    plt.subplot(132)
    plt.grid()
    yvec = np.linspace(min(y_opt[:,1]), max(y_opt[:,1]), 100)
    plt.plot(y_opt[:,1], y_pred[:,1], '.')
    plt.plot(yvec, yvec, '--')
    plt.title('y')
    plt.xlabel('Targets')

    plt.subplot(133)
    plt.grid()
    zvec = np.linspace(min(y_opt[:,2]), max(y_opt[:,2]), 100)
    plt.plot(y_opt[:,2], y_pred[:,2], '.')
    plt.plot(zvec, zvec, '--')
    plt.title('z')
    plt.xlabel('Targets')

    fig.savefig(aux.FIG_ROOT+'RegressionCurve.png')


def PlotConfusionMatrix(aux, y_pred, y_true):
    """
    This function prints and plots the confusion matrix, both normalized and not normalized.
    """

    title_norm = 'Normalized'
    title = 'Without normalization'
    classes = ['Min', 'Max']
    cmap = plt.cm.Blues

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    print('Confusion matrix, without normalization')
    print(cm)
    print()
    print("Normalized confusion matrix")
    print(cm_norm)

    fig, (ax1, ax2) = plt.subplots(1,2)
    plt.suptitle('Confusion matrix')

    # Plot confusion matrix not normalized
    im1 = ax1.imshow(cm, interpolation='nearest', cmap=cmap)
    # ax1.figure.colorbar(im1, ax=ax1)
    ax1.set(xticks=np.arange(cm.shape[1]),
        yticks=np.arange(cm.shape[0]),
        xticklabels=classes, yticklabels=classes, title=title,
        ylabel='True label', xlabel='Predicted label')

    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax1.text(j, i, format(cm[i, j], 'd'), ha="center", va="center", \
                color="white" if cm[i, j] > thresh else "black")

    # Plot confusion matrix normalized
    im2 = ax2.imshow(cm, interpolation='nearest', cmap=cmap)
    # ax2.figure.colorbar(im2, ax=ax2)
    ax2.set(xticks=np.arange(cm_norm.shape[1]),
        yticks=np.arange(cm_norm.shape[0]),
        xticklabels=classes, yticklabels=classes, title=title_norm,
        ylabel='True label', xlabel='Predicted label')

    # Loop over data dimensions and create text annotations.
    thresh = cm_norm.max() / 2.
    for i in range(cm_norm.shape[0]):
        for j in range(cm_norm.shape[1]):
            ax2.text(j, i, format(cm_norm[i, j], '.3f'), ha="center", va="center", \
                color="white" if cm_norm[i, j] > thresh else "black")

    fig.tight_layout()
    # plt.show()
    fig.savefig(aux.FIG_ROOT+'ConfusionMatrix.png')
    return fig


def PlotPredictionError3d(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        raise Exception('Net not recognized')

    scaler = pickle.load(open(aux.SCALER,'rb'))

    # test trajectories
    test_df = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_df.iloc[:, :7].values.astype('float32')
    y_r_test = test_df.iloc[:, 7:10].values.astype('float32')
    y_c_test = test_df.iloc[:, 10].values.astype('float32')
    # y_opt = test_df.iloc[:, 4].values.astype('float32')

    # predictions
    y_x_pred, y_y_pred, y_z_pred, y_c_pred = model.predict(scaler.transform(X_test))
    y_r_pred = np.c_[y_x_pred, y_y_pred, y_z_pred]
    y_c_pred = y_c_pred.argmax(axis=-1)

    y_r_err = abs(y_r_test - y_r_pred)
    y_c_err = abs(y_c_test - y_c_pred)

    N = len(X_test)
    y_pred = np.zeros((N,))

    # for i in range(N):
    #     Xi = opt_df.iloc[i, :3].values.astype('float32')
    #     y_pred[i] = model.predict(scaler.transform(Xi.reshape(1,-1)))
    #     y_pred[i] = y_pred[i].flatten()

    # y_err = abs(y_pred - y_opt)

    cmap = plt.get_cmap('jet', 100)
    # cmap.set_under('gray')

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # cax = ax.scatter(x, y, c=y_err, s=5, cmap=cmap, vmin=0.1, vmax=y_err.max())

    # plot classification error
    for i in np.arange(0,N,1):
        if y_c_err[i] or np.any(y_r_err[i,:] > 0.3):
            ax.scatter(X_test[i,0], X_test[i,1], X_test[i,2], s=5, cmap=cmap, marker='o', color='red')
        else:
            rand = np.random.uniform(0,1,1)
            if rand > 0.8:
                ax.scatter(X_test[i,0], X_test[i,1], X_test[i,2], s=5, cmap=cmap, marker='o', color='blue')

    # plot classification error
    # for i in np.arange(0,N,1):
    #     if y_c_err[i]:
    #         ax.scatter(X_test[i,0], X_test[i,1], X_test[i,2], s=5, cmap=cmap, marker='o', color='red')
    #     else:
    #         rand = np.random.uniform(0,1,1)
    #         if rand > 0.8:
    #             ax.scatter(X_test[i,0], X_test[i,1], X_test[i,2], s=5, cmap=cmap, marker='o', color='blue')

    # fig.colorbar(cax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.grid()
    plt.show()
    # fig.savefig(aux.FIG_ROOT+'PredictionError3d.png')


##########################################################################################
# HISTOGRAMS
##########################################################################################
def PlotHistogram1d(aux):
    # test trajectories
    opt_df = pd.read_csv(aux.TEST_DS, header=None)
    x_opt = opt_df.iloc[:, 0].values.astype('float32')
    y_opt = opt_df.iloc[:, 1].values.astype('float32')
    z_opt = opt_df.iloc[:, 2].values.astype('float32')
    vx_opt = opt_df.iloc[:, 3].values.astype('float32')
    vy_opt = opt_df.iloc[:, 4].values.astype('float32')
    vz_opt = opt_df.iloc[:, 5].values.astype('float32')

    # predicted trajectories
    pred_df = pd.DataFrame(util.Convert_npy2csv(aux.PRED_TRAJ))
    x_pred = pred_df.iloc[:, 0].values.astype('float32')
    y_pred = pred_df.iloc[:, 1].values.astype('float32')
    z_pred = pred_df.iloc[:, 2].values.astype('float32')
    vx_pred = pred_df.iloc[:, 3].values.astype('float32')
    vy_pred = pred_df.iloc[:, 4].values.astype('float32')
    vz_pred = pred_df.iloc[:, 5].values.astype('float32')

    n_bins = aux.n_bins
    x_bins = np.linspace(-10, 2010, n_bins+1)
    y_bins = np.linspace(-210, 210, n_bins+1)
    z_bins = np.linspace(-10, 1510, n_bins+1)

    vx_bins = np.linspace(-45, 5, n_bins+1)
    vy_bins = np.linspace(-10,10, n_bins+1)
    vz_bins = np.linspace(-35, 5, n_bins+1)
    labels = ['Optimal', 'Predicted']

    ######################################################################################
    fig, ((ax11, ax12, ax13), (ax21, ax22, ax23)) = plt.subplots(2,3, figsize=(13, 7))
    plt.suptitle('State distribution: Optimal vs Predicted')

    # n: distribution probability function
    # bins:
    # print (np.sum(n_pos_opt * np.diff(bins_pos_opt))) = 1.0
    n_x_opt, bins_x_opt, _ = ax11.hist(x_opt, bins=x_bins, density=True)
    n_x_pred, bins_x_pred, _ = ax11.hist(x_pred, bins=x_bins, density=True, alpha=0.5)
    ax11.legend(labels)
    ax11.set_ylabel('Frequency')
    ax11.set_xlabel('x [m]')

    n_y_opt, bins_y_opt, _ = ax12.hist(y_opt, bins=y_bins, density=True)
    n_y_pred, bins_y_pred, _ = ax12.hist(y_pred, bins=y_bins, density=True, alpha=0.5)
    ax12.legend(labels)
    ax12.set_xlabel('y [m]')

    n_z_opt, bins_z_opt, _ = ax13.hist(z_opt, bins=z_bins, density=True)
    n_z_pred, bins_z_pred, _ = ax13.hist(z_pred, bins=z_bins, density=True, alpha=0.5)
    ax13.legend(labels)
    ax13.set_xlabel('z [m]')

    n_vx_opt, bins_vx_opt, _ = ax21.hist(vx_opt, bins=vx_bins, density=True)
    n_vx_pred, bins_vx_pred, _ = ax21.hist(vx_pred, bins=vx_bins, density=True, alpha=0.5)
    ax21.legend(labels)
    ax21.set_ylabel('Frequency')
    ax21.set_xlabel('vx [m/s]')

    n_vy_opt, bins_vy_opt, _ = ax22.hist(vy_opt, bins=vy_bins, density=True)
    n_vy_pred, bins_vy_pred, _ = ax22.hist(vy_pred, bins=vy_bins, density=True, alpha=0.5)
    ax22.legend(labels)
    ax22.set_xlabel('vy [m/s]')

    n_vz_opt, bins_vz_opt, _ = ax23.hist(vz_opt, bins=vz_bins, density=True)
    n_vz_pred, bins_vz_pred, _ = ax23.hist(vz_pred, bins=vz_bins, density=True, alpha=0.5)
    ax23.legend(labels)
    ax23.set_xlabel('vz [m/s]')

    fig.savefig(aux.FIG_ROOT+'StateDistribution.png')

    ######################################################################################
    x_diff = abs(n_x_opt - n_x_pred) * np.diff(bins_x_opt)
    y_diff = abs(n_y_opt - n_y_pred) * np.diff(bins_y_opt)
    z_diff = abs(n_z_opt - n_z_pred) * np.diff(bins_z_opt)

    vx_diff = abs(n_vx_opt - n_vx_pred) * np.diff(bins_vx_opt)
    vy_diff = abs(n_vy_opt - n_vy_pred) * np.diff(bins_vy_opt)
    vz_diff = abs(n_vz_opt - n_vz_pred) * np.diff(bins_vz_opt)

    D_CM_x = sum(x_diff**2)
    D_CM_y = sum(y_diff**2)
    D_CM_z = sum(z_diff**2)

    D_CM_vx = sum(vx_diff**2)
    D_CM_vy = sum(vy_diff**2)
    D_CM_vz = sum(vz_diff**2)

    # fig, (ax1, ax2) = plt.subplots(1,2, figsize=(12, 6))
    # plt.suptitle('State distribution difference')
    #
    # ax1.bar(pos_bins[:-1], height=pos_diff, edgecolor='black', linewidth=1.2,
    # width=(pos_bins[1]-pos_bins[0]), align='edge')
    # ax1.set_ylabel('Frequency')
    # ax1.set_xlabel('Position [m]')
    #
    # ax2.bar(vel_bins[:-1], height=vel_diff, edgecolor='black', linewidth=1.2,
    # width=(vel_bins[1]-vel_bins[0]), align='edge')
    # ax2.set_xlabel('Velocity [m/s]')
    #
    # fig.savefig(FIG_ROOT+'StateDistributionDifference.png')
    return [D_CM_x, D_CM_y, D_CM_z, D_CM_vx, D_CM_vy, D_CM_vz]


def PlotHistograms(aux):
    # test trajectories
    opt_df = pd.read_csv(aux.TEST_DS, header=None)
    X_opt = opt_df.iloc[:, :6].values.astype('float32')

    # predicted trajectories
    pred_df = pd.DataFrame(util.Convert_npy2csv(aux.PRED_TRAJ))
    X_pred = pred_df.iloc[:, :6].values.astype('float32')

    n_bins = 10

    ######################################################################################
    fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(12, 6))

    n_opt, (bins_x_opt, bins_y_opt, bins_z_opt, bins_vx_opt, bins_vy_opt, bins_vz_opt) = np.histogramdd(X_opt, bins=n_bins, normed=True)
    n_pred, (bins_x_pred, bins_y_pred, bins_z_pred, bins_vx_pred, bins_vy_pred, bins_vz_pred) = np.histogramdd(X_pred, bins=n_bins, normed=True)

    # A = n_opt * np.diff(bins_x_opt) * np.diff(bins_y_opt) * np.diff(bins_z_opt) * np.diff(bins_vx_opt) * np.diff(bins_vy_opt) * np.diff(bins_vz_opt)
    # print( np.sum(A))
    # A = n_pred * np.diff(bins_x_pred) * np.diff(bins_y_pred) * np.diff(bins_z_pred) * np.diff(bins_vx_pred) * np.diff(bins_vy_pred) * np.diff(bins_vz_pred)
    # print( np.sum(A))

    ######################################################################################
    diff_distr = abs(n_opt - n_pred) * np.diff(bins_x_opt) * np.diff(bins_y_opt) * np.diff(bins_z_opt) * np.diff(bins_vx_opt) * np.diff(bins_vy_opt) * np.diff(bins_vz_opt)
    D_CM = np.sum(diff_distr**2)
    return D_CM


##########################################################################################
# OTHER UTILS
##########################################################################################
def PlotTrainHistory(aux):
    history = pickle.load(open(aux.TRAIN_HISTORY, 'rb'))
    h = history.history
    labels = ['Train', 'Validation']
    epochs = np.arange(0, len(h['lr']), 1)

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(12,6))
    ax1.plot(epochs[10:], h['loss'][10:])
    ax1.plot(epochs[10:], h['val_loss'][10:])
    ax1.set_ylabel('Loss')
    ax1.set_xlabel('Epoch')
    ax1.legend(labels)

    ax2.plot(epochs[10:], h['mean_squared_error'][10:])
    ax2.plot(epochs[10:], h['val_mean_squared_error'][10:])
    ax2.set_ylabel('MSE')
    ax2.set_xlabel('Epoch')
    ax2.legend(labels)

    ax3.plot(epochs, h['lr'])
    ax3.set_ylabel('lr')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'TrainHistory.png')


def PlotTrainValLossELM(aux, rmse_train_vec, rmse_val_vec, neurons_list):
    # rmse_train_vec = np.array(rmse_train_vec)
    fig = plt.figure()
    plt.plot(neurons_list, rmse_train_vec)
    plt.plot(neurons_list, rmse_val_vec)
    plt.legend(['Train loss', 'Validation loss'])
    plt.xlabel('Number of neurons')
    plt.ylabel('Loss (RMSE)')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'TrainValLoss.png')


def render_mpl_table(data, col_width=3.0, row_height=0.825, font_size=15):
    header_color = '#40466e'
    row_colors = ['#f1f1f2', 'w']
    edge_color = 'w'
    bbox = [0, 0, 1, 1]
    header_columns = 0

    size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
    fig, ax = plt.subplots(figsize=size)
    ax.axis('off')

    mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns)

    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)

    for k, cell in  six.iteritems(mpl_table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
    return fig, ax
