Dagger iteration: 1

Train set points:	1.06E+04
Test RMSE:		3.30E-01

Monte Carlo simulation:
Success rate:		5.00 %
Average final position:	[5.41, 3.67, 10.01]
Average final velocity:	[3.83, 1.25, 1.94]

Distance CM:		5.73E-03