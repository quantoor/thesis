Dagger iteration: 3

Train set points:	1.37E+04
Test RMSE:		3.24E-01

Monte Carlo simulation:
Success rate:		10.00 %
Average final position:	[23.02, 7.45, 0.45]
Average final velocity:	[0.97, 2.01, 9.04]

Distance CM:		4.44E-03