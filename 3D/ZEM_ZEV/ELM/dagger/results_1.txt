Dagger iteration: 1

Train set points:	1.06E+04
Test loss:		6.70E-04

Monte Carlo simulation:
Success rate:		0.00 %
Average final position:	[0.65, 0.22, 0.04]
Average final velocity:	[2.24, 0.14, 0.80]

Distance CM:		6.63E-04