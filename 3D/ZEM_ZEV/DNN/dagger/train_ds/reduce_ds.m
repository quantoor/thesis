clear
clc


ds_1000 = csvread('ds_train_1000.csv');
ds_reduced = ds_1000(1:30000,:);

csvwrite('ds_train_0.csv', ds_reduced)


figure()
hold on
grid on
plot3(ds_reduced(:,1),ds_reduced(:,2),ds_reduced(:,3),'b.')
axis equal