clc
clear
close all


old_ds = csvread('ds_train_3.csv');
new_traj = csvread('new_trajectories_3.csv');
 
figure()
hold on
plot(old_ds(:,1), old_ds(:,2),'b.')
plot(new_traj(:,1), new_traj(:,2),'r.')


fprintf('Max altitude:\n')
fprintf('%.4f\n', max(old_ds(:,1)))
fprintf('%.4f\n\n', max(new_traj(:,1)))

fprintf('Min altitude:\n')
fprintf('%.4f\n', min(old_ds(:,1)))
fprintf('%.4f\n\n', min(new_traj(:,1)))

fprintf('Max velocity:\n')
fprintf('%.4f\n', max(old_ds(:,2)))
fprintf('%.4f\n\n', max(new_traj(:,2)))

fprintf('Min velocity:\n')
fprintf('%.4f\n', min(old_ds(:,2)))
fprintf('%.4f\n\n', min(new_traj(:,2)))

fprintf('Max acc:\n')
fprintf('%.4f\n', max(old_ds(:,4)))
fprintf('%.4f\n\n', max(new_traj(:,4)))

fprintf('Min acc:\n')
fprintf('%.4f\n', min(old_ds(:,4)))
fprintf('%.4f\n\n', min(new_traj(:,4)))