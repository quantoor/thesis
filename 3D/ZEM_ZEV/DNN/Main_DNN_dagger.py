import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt
import pickle
import time
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dense
from sklearn.preprocessing import MinMaxScaler
import argparse
import scipy.optimize
from numpy.random import seed
from tensorflow import set_random_seed

import sys
sys.path.insert(0, '../../common3D')
import Eom, util, util_plot, ZEM_ZEV_3D


def TrainNetwork(aux, FLAGS, overwrite=False):
    seed(42)
    set_random_seed(42)
    if not FLAGS.hyper_mode:
        try:
            load_model(aux.MODEL)
            if not overwrite:
                print('Model already exists, exit training.\n')
                return
        except:
            pass

    layers = FLAGS.layers
    n_hidden = FLAGS.n_hidden
    lr = FLAGS.lr
    batch_size = FLAGS.batch_size
    epochs = FLAGS.epochs
    l2 = FLAGS.l2
    hyper_mode = FLAGS.hyper_mode
    # =============================================================================
    #  Data
    # =============================================================================
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    X_train = train_ds.iloc[:, :7].values      # state: 3 pos, 3 vel and tgo
    y_train = train_ds.iloc[:, 8:11].values    # target: 3 acc

    scaler = MinMaxScaler(feature_range=(0,100))
    X_train = scaler.fit_transform(X_train)

    # save scaler
    if not hyper_mode:
        pickle.dump(scaler, open(aux.SCALER, 'wb'))

    # =============================================================================
    # DNN
    # =============================================================================
    initializer = keras.initializers.uniform()
    regularizer = keras.regularizers.l2(l2)

    visible = Input(shape=(7,), name='main_input')

    if layers == 2:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
    elif layers == 4:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)
    elif layers == 5:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_4 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_4)
    elif layers == 6:
        hidden_1 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(visible)
        hidden_2 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_1)
        hidden_3 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_2)
        hidden_4 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_3)
        hidden_5 = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_4)
        hidden_o = Dense(n_hidden, activation='relu', kernel_initializer=initializer, kernel_regularizer=regularizer)(hidden_5)

    x = Dense(1, activation='relu', name='x')(hidden_o)
    y = Dense(1, activation='tanh', name='y')(hidden_o)
    z = Dense(1, activation='relu', name='z')(hidden_o)

    model = Model(inputs=[visible], outputs=[x, y, z])

    # =============================================================================
    # Train model
    # =============================================================================
    optimizer = keras.optimizers.Adam(lr=lr, amsgrad=True)
    # optimizer = keras.optimizers.Adagrad(lr=lr)

    model.compile(
        loss={'x':'mean_squared_error', 'y':'mean_squared_error', 'z':'mean_squared_error'},
        metrics={'x':'mse', 'y':'mse', 'z':'mse'},
        optimizer=optimizer)

    call_backs = util.GetCallBacks(aux, FLAGS)

    history = model.fit(
        {'main_input': X_train},
        {'x': y_train[:,0], 'y': y_train[:,1], 'z': y_train[:,2]},
        validation_split=0.15, epochs=epochs, batch_size=batch_size,
        verbose=2, callbacks=call_backs)

    if not hyper_mode:
        pickle.dump(history, open(aux.TRAIN_HIST, "wb"))


def Main(FLAGS):
    # =============================================================================
    #  Main parameters
    # =============================================================================
    type = 'dagger'
    net = 'DNN'
    if FLAGS.iter>-1:
        iter = str(FLAGS.iter)
    else:
        iter = '0'
    dist = False
    start_point = 0.9
    d_bc = 1
    aux = util.Aux(type, net, iter, d_bc, dist, start_point=start_point)

    # =============================================================================
    #  Main
    # =============================================================================
    # TrainNetwork(aux, FLAGS, overwrite=True)
    # rmse = util.ZEM_ZEV_EvaluateNetwork(aux)
    #
    # util.ZEM_ZEV_MonteCarloSimulation(aux, overwrite=True)
    # util.ZEM_ZEV_GetResults(aux, rmse)

    # util.ZEM_ZEV_CollectStatesToCorrect(aux)
    # util.ZEM_ZEV_CollectBCForDAgger(aux, plot=0)

    model = load_model(aux.MODEL)
    print(model.summary())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--iter', type=int, default=-1, help='iteration')
    parser.add_argument('--layers', type=int, default=5, help='hidden layers')
    parser.add_argument('--n_hidden', type=int, default=256, help='hidden neurons')
    parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
    parser.add_argument('--batch_size', type=int, default=8, help='mini batch size')
    parser.add_argument('--epochs', type=int, default=500, help='epochs')
    parser.add_argument('--l2', type=float, default=1e-6, help='L2 penalty regularization')
    parser.add_argument('--hyper_mode', type=bool, default=False, help='If true, just compare different models')
    parser.add_argument('--custom_name', type=str, default=None, help='custom name for tensorboard folder')

    FLAGS = parser.parse_args()
    Main(FLAGS)
