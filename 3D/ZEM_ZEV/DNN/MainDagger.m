clear; close all; clc
addpath('../../common3D/matlab')

dagger_it = 2;

ROOT = 'dagger/train_ds/';
TRAIN_OLD = strcat(ROOT, 'ds_train_', num2str(dagger_it), '.csv'); 
TRAIN_NEW = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.csv');
TEXT = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.txt');
BC4DAGGER = strcat(ROOT, 'bc4dagger_', num2str(dagger_it), '.csv');
NEW_TRAJ = strcat(ROOT, 'new_trajectories_', num2str(dagger_it), '.csv');

BC_mat = csvread(BC4DAGGER);
train_ds = csvread(TRAIN_OLD);

%% plot old train set and new states
figure()
hold on
grid on
plot3(train_ds(1:10:end,1),train_ds(1:10:end,2),train_ds(1:10:end,3),'b.')
plot3(BC_mat(:,1),BC_mat(:,2),BC_mat(:,3),'r.')
axis equal
xlim([-10 2000])
ylim([-300 300])
zlim([-5 1500])


%% ZEM ZEV DAgger
N = size(BC_mat, 1);


aux = [];
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    BC = BC_mat(i, 1:6);
    
    % Compute trajectory
    try
        tic
        [state, acc, time] = ZEM_ZEV_3D(BC);    
        toc
    catch
        disp('Trajectory skipped')
        skipped_trajectories = skipped_trajectories + 1;
        continue
    end
    
    % Preprocess data        
    Mat_ds = [state, acc, time];
    Mat_ds = Mat_ds(1, :);

    aux = [aux; Mat_ds]; 
    
    ppm.increment()
end


%%
ds_augmented = [train_ds; aux];
ds_augmented(ds_augmented(:,3)<0, :) = []; % remove points with negative altitude
ds_augmented_shuffled = randblock(ds_augmented, [1, size(aux,2)]); % shuffle

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)
fprintf('\nOld training set: %d states\n', size(train_ds,1))
fprintf('New states: %d states\n', size(aux,1));
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))
fprintf('Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1))

logFileID = fopen(TEXT, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Skipped trajectories: %d\n', skipped_trajectories);
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fprintf(logFileID, 'Augmented training set shuffled: %d states\n', size(ds_augmented_shuffled,1));
fclose(logFileID);

dlmwrite(TRAIN_NEW, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)
dlmwrite(NEW_TRAJ, aux, 'delimiter', ',', 'precision', 8)
rmpath('../../common3D/matlab')
