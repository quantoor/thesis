clear; close all; clc
rng('shuffle')
addpath('../../common3D/matlab')

% Number of trajectories
N = 1000;

ROOT = 'dagger/train_ds/';
FILE_NAME = 'ds_train_0';
SAVE_PATH = strcat(ROOT, FILE_NAME, '.csv');

aux = [];
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 60);

parfor i = 1:N
    % Random BC
    x0_rand = unifrnd(1500, 2000);
    y0_rand = unifrnd(-100, 100);
    z0_rand = unifrnd(1000, 1500);
    vx0_rand = unifrnd(-60, -50);
    vy0_rand = unifrnd(-10, 10);
    vz0_rand = unifrnd(-30, -20);
    BC = [x0_rand, y0_rand, z0_rand, vx0_rand, vy0_rand, vz0_rand];

    % Compute trajectory
    try
        tic
        [state, acc, time] = ZEM_ZEV_3D(BC);    
        toc
    catch
        skipped_trajectories = skipped_trajectories + 1;
    end
        
    % Avoid trajectories with downward thrust
    if acc(1, 3) < 0
        disp('Downward thrust, skip this trajectory')
        disp(BC)
        continue
    end

    % Preprocess data       
    Mat_ds = [state, acc, time];
    Mat_ds = Mat_ds(1:end-2,:); % remove last states 

    aux = [aux; Mat_ds];

%     figure()
%     subplot(411)
%     plot(time, state(:,1), tfix, Xfix(:,1),'.')
%     ylabel('Altitude [m]')
%     subplot(412)
%     plot(time, state(:,2), tfix, Xfix(:,2),'.')
%     ylabel('Velocity [m/s]')
%     subplot(413)
%     plot(time, state(:,3), tfix, Xfix(:,3),'.')
%     ylabel('Mass [kg]')
%     subplot(414)
%     plot(time, acc, tfix, yfix,'.')
%     ylabel('Acceleration [m/s^2]')
    ppm.increment()
end

n_points = size(aux,1);

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

aux = randblock(aux, [1, size(aux,2)]);
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)
logFileID = fopen(strcat(ROOT, FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);
rmpath('../../common3D/matlab')

%% plot 3D
close all
x = aux(:,1);
y = aux(:,2);
z = aux(:,3);

figure()
plot3(x, y, z, '.')
grid on
xlabel('x')
ylabel('y')
zlabel('z')
axis equal