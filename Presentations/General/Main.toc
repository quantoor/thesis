\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}1D problem}{2}% 
\contentsline {subsection}{\numberline {1.0.1}Definitions}{2}% 
\contentsline {section}{\numberline {1.1}ZEM ZEV}{2}% 
\contentsline {subsection}{\numberline {1.1.1}Integration with optimal control}{2}% 
\contentsline {subsection}{\numberline {1.1.2}Dataset generation}{3}% 
\contentsline {subsection}{\numberline {1.1.3}Dagger criteria}{3}% 
\contentsline {subsection}{\numberline {1.1.4}DNN - Without mass as input feature}{4}% 
\contentsline {subsection}{\numberline {1.1.5}DNN - With mass as input feature}{7}% 
\contentsline {section}{\numberline {1.2}GPOPS}{10}% 
\contentsline {subsection}{\numberline {1.2.1}Sensitivity analysis on the switching time}{10}% 
\contentsline {subsection}{\numberline {1.2.2}Dataset generation}{11}% 
\contentsline {subsection}{\numberline {1.2.3}Dagger criteria}{11}% 
\contentsline {subsection}{\numberline {1.2.4}DNN - Without mass as input feature}{12}% 
\contentsline {subsection}{\numberline {1.2.5}DNN - With mass as input feature}{15}% 
\contentsline {section}{\numberline {1.3}LT2.0}{18}% 
